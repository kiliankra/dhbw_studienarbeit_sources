Das Verzeichnis `latest` enthält immer die Konfiguration, bzw. die Gewichte, die in die Docker-Images gepackt werden.
Dies entspricht nicht unbedingt immer dem aktuellsten Stand Version, sondern einfach dem Stand, der zum aktuellen Zeitpunkt am besten funktioniert.

Aktueller Stand: `v3`