#!/bin/bash
#
# Script to setup, prepare and initiate training
#
# This should work on most systems with APT (e.g. Jetpack, Ubuntu, Debian).
# Necessary: ~53 GB of free space for the default classes and  both datasets (therefore an SD-Card with 128 GB is recommended for the Jetson Nano)
#

echo "Preparing system for training..."

echo "Checking CUDA availability"
CUDA_COMPILER=$(which nvcc)
if [ -z "$CUDA_COMPILER" ]; then
    echo "CUDA is required for training (since it would take way too long without it). Please run this script on a system with CUDA installed"
    exit 1
else
    echo "CUDA is available."
fi

echo "Installing dependencies"
sudo apt update
sudo apt install python3 python3-pip git make cmake gcc g++ pkg-config

echo "Checking Darknet/YOLO Availability"
DARKNET_EXECUTABLE=$(which darknet)
if [ -z "$DARKNET_EXECUTABLE" ]; then
    echo "Downloading/Compiling YOLO Program"
    echo ". Cloning YOLO repository"
    git clone https://github.com/AlexeyAB/darknet.git
    cd darknet
    mkdir build_dir
    cd build_dir

    echo ". Configuring with CMake"
    cmake -D ENABLE_CUDA=ON ..
    echo ". Running make to compile"
    make -j$(nproc)
    cd ../..
    DARKNET_EXECUTABLE=./darknet/build_dir/darknet
fi


echo "Creating necessary directories"
mkdir training
mkdir training/backup
mkdir export
mkdir export/validation

echo "Downloading necessary files from YOLO"
wget -O training/mintenv-start.weights https://github.com/AlexeyAB/darknet/releases/download/yolov4/yolov4-tiny.conv.29

DOCKER_EXECUTABLE=$(which docker)
if [ -z "$DOCKER_EXECUTABLE" ]; then
	echo "Docker is not available therefore the local python3 installation is used"
	echo "Installing necessary python packages"
  python3 -m pip install --upgrade pip setuptools wheel
  python3 -m pip install fiftyone

  echo "Running python3 to prepare the data"
  python3 ./prepare_training.py
else
	echo "Running data preparation in a docker container"
	docker run --rm -v $PWD:$PWD --workdir=$PWD -it menkalian/dhbw_studienarbeit_training_prep:latest python3 ./prepare_training.py
fi


echo "Starting training. This will take a long time."
echo ".  To restart training if it fails use the following command (with the appropriate values) to resume training from the last savepoint"
echo ".  $ ./darknet/build_dir/darknet detector train training/obj.data training/mintenv.cfg training/backup/mintenv-start-XXXXX.weights -map -dont_show"
$DARKNET_EXECUTABLE detector train training/obj.data training/mintenv.cfg training/mintenv-start.weights -map -dont_show
