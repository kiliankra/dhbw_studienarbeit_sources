#!/usr/bin/python

import click
import fiftyone
import fiftyone.zoo
import json
import os
import sys


def load_config(file):
    f = open(file)
    return json.load(f)


def ask_dataset(dataset_name):
    return click.confirm('Do you want to use the "' + dataset_name + '"-Dataset?', default=True)


def get_coco_dataset_id():
    return 'coco'


def get_open_images_dataset_id():
    return 'oid'


def get_configured_classes_list(config, dataset_id):
    classes = []
    for i in config['classes']:
        cs = config['classes'][i][dataset_id]
        for c in cs:
            classes.append(c)
    return classes


def get_target_classes(config):
    classes = []
    for c in config['classes']:
        classes.append(c)
    return classes


def build_translation_dict(config, dataset_id):
    translation = {}
    for k in config['classes']:
        cs = config['classes'][k][dataset_id]
        for c in cs:
            translation[c] = k
    return translation


if __name__ == '__main__':
    argcount = len(sys.argv)
    if argcount >= 2:
        print('config file: ', sys.argv[1])
        config = load_config(sys.argv[1])
    else:
        config = load_config('training_config.json')

    is_coco = ask_dataset('COCO')
    is_oid = ask_dataset('Open Images')

    # Set fiftyone download dir to the current directory

    fiftyone.config.dataset_zoo_dir = os.getcwd() + '/fiftyone'
    export_dir = os.getcwd() + '/export'

    data_count = 0

    if is_coco:
        # Detections are stored in 'ground_truth'
        coco_data = fiftyone.zoo.load_zoo_dataset(
            'coco-2017',
            splits=['train', 'test'],
            label_types=['detections'],
            classes=get_configured_classes_list(config, get_coco_dataset_id()))
        coco_validation = fiftyone.zoo.load_zoo_dataset(
            'coco-2017',
            splits=['validation'],
            label_types=['detections'],
            classes=get_configured_classes_list(config, get_coco_dataset_id()))

        coco_data.persistent = True
        coco_validation.persistent = True

        coco_mapped_dataset = coco_data.filter_labels(
            'ground_truth',
            fiftyone.ViewField('label').is_in(get_configured_classes_list(config, get_coco_dataset_id()))
        ).map_labels('ground_truth', build_translation_dict(config, get_coco_dataset_id()))
        coco_mapped_validation = coco_validation.filter_labels(
            'ground_truth',
            fiftyone.ViewField('label').is_in(get_configured_classes_list(config, get_coco_dataset_id()))
        ).map_labels('ground_truth', build_translation_dict(config, get_coco_dataset_id()))

        data_count += coco_mapped_dataset.count()
        coco_export_dir = export_dir + '/coco'
        training_dir = coco_export_dir

        coco_mapped_dataset.export(
            coco_export_dir,
            label_field='ground_truth',
            dataset_type=fiftyone.types.YOLOv4Dataset,
            classes=get_target_classes(config))
        coco_mapped_validation.export(
            coco_export_dir + '/validation',
            label_field='ground_truth',
            dataset_type=fiftyone.types.YOLOv4Dataset,
            classes=get_target_classes(config))

        # Create a shared file, which lists the data
        images = os.listdir(coco_export_dir + '/data')
        images_list_file = open(coco_export_dir + '/images.txt', 'w')
        for image in filter(lambda f: f.endswith('.jpg'), images):
            images_list_file.write(coco_export_dir + '/data/' + image + '\n')

        # Create a shared file, which lists the data
        validations = os.listdir(coco_export_dir + '/validation/data')
        images_list_file = open(coco_export_dir + '/validation/images.txt', 'w')
        for image in filter(lambda f: f.endswith('.jpg'), validations):
            images_list_file.write(coco_export_dir + '/validation/data/' + image + '\n')

    if is_oid:
        # Achtung RAM
        oid_data = fiftyone.zoo.load_zoo_dataset(
            'open-images-v6',
            splits=['train', 'test'],
            label_types=['detections'],
            classes=get_configured_classes_list(config, get_open_images_dataset_id()))
        oid_validation = fiftyone.zoo.load_zoo_dataset(
            'open-images-v6',
            splits=['validation'],
            label_types=['detections'],
            classes=get_configured_classes_list(config, get_open_images_dataset_id()))

        oid_data.persistent = True
        oid_validation.persistent = True

        oid_mapped_dataset = oid_data.filter_labels(
            'detections',
            fiftyone.ViewField('label').is_in(get_configured_classes_list(config, get_open_images_dataset_id()))
        ).map_labels('detections', build_translation_dict(config, get_open_images_dataset_id()))
        oid_mapped_validation = oid_validation.filter_labels(
            'detections',
            fiftyone.ViewField('label').is_in(get_configured_classes_list(config, get_open_images_dataset_id()))
        ).map_labels('detections', build_translation_dict(config, get_open_images_dataset_id()))

        data_count += oid_mapped_dataset.count()
        oid_export_dir = export_dir + '/open-images'
        training_dir = oid_export_dir

        oid_mapped_dataset.export(
            oid_export_dir,
            label_field='detections',
            dataset_type=fiftyone.types.YOLOv4Dataset,
            classes=get_target_classes(config))
        oid_mapped_validation.export(
            oid_export_dir + '/validation',
            label_field='detections',
            dataset_type=fiftyone.types.YOLOv4Dataset,
            classes=get_target_classes(config))

        # Create a shared file, which lists the data
        images = os.listdir(oid_export_dir + '/data')
        images_list_file = open(oid_export_dir + '/images.txt', 'w')
        for image in filter(lambda f: f.endswith('.jpg'), images):
            images_list_file.write(oid_export_dir + '/data/' + image + '\n')

        # Create a shared file, which lists the data
        validations = os.listdir(oid_export_dir + '/validation/data')
        images_list_file = open(oid_export_dir + '/validation/images.txt', 'w')
        for image in filter(lambda f: f.endswith('.jpg'), validations):
            images_list_file.write(oid_export_dir + '/validation/data/' + image + '\n')

    print('Preparing data complete. Configuring training')

    if is_coco and is_oid:
        # reset training_dir to use both exported datasets
        training_dir = export_dir

        # write a classes list
        objects_file = open(export_dir + '/obj.names', 'w')
        for c in get_target_classes(config):
            objects_file.write(c + '\n')

        # Create a shared file, which lists the data
        images1 = os.listdir(coco_export_dir + '/data')
        images2 = os.listdir(oid_export_dir + '/data')

        images_list_file = open(export_dir + '/images.txt', 'w')
        for image in filter(lambda f: f.endswith('.jpg'), images1):
            images_list_file.write(coco_export_dir + '/data/' + image + '\n')
        for image in filter(lambda f: f.endswith('.jpg'), images2):
            images_list_file.write(oid_export_dir + '/data/' + image + '\n')

        # Create a shared file, which lists the data
        validations1 = os.listdir(coco_export_dir + '/validation/data')
        validations2 = os.listdir(oid_export_dir + '/validation/data')

        images_list_file = open(export_dir + '/validation/images.txt', 'w')
        for image in filter(lambda f: f.endswith('.jpg'), validations1):
            images_list_file.write(coco_export_dir + '/validation/data/' + image + '\n')
        for image in filter(lambda f: f.endswith('.jpg'), validations2):
            images_list_file.write(oid_export_dir + '/validation/data/' + image + '\n')

    if not is_oid and not is_coco:
        print('No dataset was selected. All previously exported datasets will be used.')
        training_dir = export_dir

    # create obj.data file

    obj_data_file = open(os.getcwd() + '/training/obj.data', 'w')
    obj_data_file.write('classes = ' + str(len(get_target_classes(config))) + '\n')
    obj_data_file.write('names = ' + training_dir + '/obj.names\n')
    obj_data_file.write('train = ' + training_dir + '/images.txt\n')
    obj_data_file.write('valid = ' + training_dir + '/validation/images.txt\n')
    obj_data_file.write('backup = ' + os.getcwd() + '/training/backup\n')

    # Load config template
    class_count = len(get_target_classes(config))
    max_batch_count = class_count * 2000

    if max_batch_count < 6000:
        max_batch_count = 6000

    if max_batch_count < data_count:
        max_batch_count = data_count

    replacement_dict = {
        '{{Classes.Amount}}':            str(class_count),
        '{{Classes.Filter.Amount}}':     str((class_count + 5) * 3),
        '{{Batches.Max}}':               str(max_batch_count),
        '{{Batches.Percentile.Eighty}}': str(int(0.8* max_batch_count)),
        '{{Batches.Percentile.Ninety}}': str(int(0.9 * max_batch_count)),
    }

    cfg_template_file = open('mintenv.cfg.template', 'r')
    cfg_template = cfg_template_file.read()

    for placeholder in replacement_dict:
        cfg_template = cfg_template.replace(placeholder, replacement_dict[placeholder])

    cfg_file = open(os.getcwd() + '/training/mintenv.cfg', 'w')
    cfg_file.write(cfg_template)
