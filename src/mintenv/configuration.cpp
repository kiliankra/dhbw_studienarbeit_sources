/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#include "configuration.h"

#include <cstdlib>
#include <iostream>
#include <sstream>

// Convenience Macros for prettier formatting
#define READ_FROM_ENV(PROP, ENV_VAR, DEFAULT) \
    PROP = getFromEnv( ENV_VAR , DEFAULT );

// Parses the integer value from the associated string property
#define PARSE_INT(PROP) \
    PROP = std::stoi( PROP ## Str );

// Parses the float value from the associated string property
#define PARSE_FLOAT(PROP) \
    PROP = std::atof( PROP ## Str.c_str() );

LogLevel fromString(std::string level) {
    if (level.compare("ERROR") == 0) {
        return LogLevel::ERROR;
    }
    if (level.compare("WARN") == 0) {
        return LogLevel::WARN;
    }
    if (level.compare("INFO") == 0) {
        return LogLevel::INFO;
    }
    if (level.compare("DEBUG") == 0) {
        return LogLevel::DEBUG;
    }
    if (level.compare("TRACE") == 0) {
        return LogLevel::TRACE;
    }
    throw std::runtime_error("Unknown LogLevel " + level);
}

Configuration::Configuration() : logger("Config") {
    logger.info("Lade Konfiguration.");

    /*             property                    environment variable       default value                   */
    READ_FROM_ENV(logMaxLevel, "LOG_MAX_LEVEL", "DEBUG")

    READ_FROM_ENV(sourceType, "SOURCE_TYPE", "WEBCAM")
    READ_FROM_ENV(sourceWebcamDeviceNoStr, "SOURCE_VIDEO_DEVICE_NO", "0")
    READ_FROM_ENV(sourceResolutionWidthStr, "SOURCE_RES_WIDTH", "1920")
    READ_FROM_ENV(sourceResolutionHeightStr, "SOURCE_RES_HEIGHT", "1080")

    READ_FROM_ENV(detectionThresholdStr, "DETECTION_THRESHOLD", "0.2")
    READ_FROM_ENV(detectionMaxBoxOverlapStr, "DETECTION_MAX_OVERLAP", "0.4")
    READ_FROM_ENV(detectionBlobSizeStr, "DETECTION_BLOB_SIZE", "832")

    READ_FROM_ENV(yoloConfigurationFile, "YOLO_CONFIG_FILE", "/etc/mintenv/yolo/yolo.cfg")
    READ_FROM_ENV(yoloWeightsFile, "YOLO_WEIGHTS_FILE", "/etc/mintenv/yolo/yolo.weights")
    READ_FROM_ENV(yoloNamesFile, "YOLO_NAMES_FILE", "/etc/mintenv/yolo/yolo.names")

    READ_FROM_ENV(targetResolutionWidthStr, "TARGET_RES_WIDTH", getSourceResolutionWidthStr())
    READ_FROM_ENV(targetResolutionHeightStr, "TARGET_RES_HEIGHT", getSourceResolutionHeightStr())
    READ_FROM_ENV(targetFramerateStr, "TARGET_FRAMERATE", "20" /* should be a divisor for 1000 */ )

    READ_FROM_ENV(streamHost, "STREAM_HOST", "127.0.0.1")
    READ_FROM_ENV(streamPort, "STREAM_PORT", "8554")
    READ_FROM_ENV(streamPath, "STREAM_PATH", "mintenv")
    READ_FROM_ENV(streamUser, "STREAM_USER", "")
    READ_FROM_ENV(streamPassword, "STREAM_PASS", "")

    // Parse the values for all integer fields
    PARSE_INT(sourceWebcamDeviceNo)
    PARSE_INT(sourceResolutionWidth)
    PARSE_INT(sourceResolutionHeight)

    PARSE_FLOAT(detectionThreshold)
    PARSE_FLOAT(detectionMaxBoxOverlap)
    PARSE_INT(detectionBlobSize)

    PARSE_INT(targetResolutionWidth)
    PARSE_INT(targetResolutionHeight)
    PARSE_INT(targetFramerate)

    // Build the default Stream URL from the values above
    std::string defaultStreamUrl("rtsp://");

    // Add credentials if provided
    if (!streamUser.empty()) {
        defaultStreamUrl += getStreamUser();

        if (!streamPassword.empty())
            defaultStreamUrl += ":" + getStreamPassword();

        defaultStreamUrl += "@";
    }
    defaultStreamUrl += getStreamHost() + ":" + getStreamPort() + "/" + getStreamPath();

    // Allow overwriting the default URL with an environment variable
    READ_FROM_ENV(streamUrl, "STREAM_URL", defaultStreamUrl)

    Logger::setMaxLogLevel(fromString(logMaxLevel));
    print();
}

void Configuration::print() const {
    std::stringstream logLine;
    logLine << "Geladene Konfiguration:\n"
            << "  Logging:\n"
            << "    Max. Level:       " << logMaxLevel << "\n"
            << "\n"
            << "  Quelle:\n"
            << "    Typ:              " << sourceType << "\n"
            << "    Auflösung:        " << sourceResolutionWidthStr << "x" << sourceResolutionHeightStr << "\n"
            << "    Kameranummer:     " << sourceWebcamDeviceNoStr << "\n"
            << "\n"
            << "  YOLO:\n"
            << "    Konfiguration:    " << yoloConfigurationFile << "\n"
            << "    Gewichte:         " << yoloWeightsFile << "\n"
            << "    Klassen:          " << yoloNamesFile << "\n"
            << "\n"
            << "  Objektdetektion:\n"
            << "    Erkennungsgrenze: " << detectionThresholdStr << "\n"
            << "    Max. Überlappung: " << detectionMaxBoxOverlapStr << "\n"
            << "    Blob-Größe:       " << detectionBlobSizeStr << "\n"
            << "\n"
            << "  Videoausgabe:\n"
            << "    Auflösung:        " << targetResolutionWidthStr << "x" << targetResolutionHeightStr << "\n"
            << "    Bildrate (Ziel):  " << targetFramerate << "\n"
            << "\n"
            << "  Stream:\n"
            << "    URL (Ziel):       " << streamUrl << "\n";

    logger.info(logLine.str());
}

std::string Configuration::getFromEnv(const std::string &envVarName, const std::string &defaultVal) {
    auto value = std::getenv(envVarName.c_str());
    return value == nullptr ? defaultVal : std::string(value);
}
