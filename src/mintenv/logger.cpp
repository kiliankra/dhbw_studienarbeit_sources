/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#include "logger.h"

#include <ctime>
#include <iostream>
#include <iomanip>
#include <thread>

#include <string>

Logger::Logger(std::string loggerName) : loggerName(std::move(loggerName)) {}

void Logger::trace(std::string line) const {
    log(TRACE, line);
}

void Logger::debug(std::string line) const {
    log(DEBUG, line);
}

void Logger::info(std::string line) const {
    log(INFO, line);
}

void Logger::warn(std::string line) const {
    log(WARN, line);
}

void Logger::error(std::string line) const {
    log(ERROR, line);
}

std::string getLevelString(LogLevel lvl) {
    switch (lvl) {
        case TRACE:
            return "< TRACE  >";
        case DEBUG:
            return "< DEBUG  >";
        case INFO:
            return "< INFO   >";
        case WARN:
            return "< WARN   >";
        case ERROR:
            return "< ERROR  >";
    }

    return "< UNKNOWN>";
}

std::string getColor(LogLevel lvl) {
#ifdef ANSI_COLOR
    switch (lvl) {
        case TRACE:
            return "\u001B[38;2;100;100;100m";
        case DEBUG:
            return "\u001B[38;2;110;70;110m";
        case INFO:
            return "\u001B[38;2;10;110;210m";
        case WARN:
            return "\u001B[38;2;210;140;10m";
        case ERROR:
            return "\u001B[38;2;200;20;0m";
    }
#endif

    return "";
}

std::string getColorReset() {
#ifdef ANSI_COLOR
    return "\u001B[39m";
#else
    return "";
#endif
}

void Logger::log(LogLevel level, std::string &line) const {
    if (level < getMaxLogLevel())
        return;

    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    // Treat stdout as semaphore
    std::lock_guard<std::mutex> logGuard(loggerMutex);
    std::cout
            << getColor(level)
            << std::put_time(&tm, "%Y-%m-%d %H:%M:%S") << " | "
            << "TID:[" << std::this_thread::get_id() << "] "
            << getLevelString(level) << " "
            << "[" << std::setw(20) << loggerName << "]: "
            << line
            << getColorReset()
            << std::endl;
}

LogLevel Logger::maxLogLevel = TRACE;

LogLevel Logger::getMaxLogLevel() {
    return maxLogLevel;
}

void Logger::setMaxLogLevel(LogLevel maxLogLevel) {
    Logger::maxLogLevel = maxLogLevel;
}

// Implement mutex here to avoid linker error
std::mutex Logger::loggerMutex;
