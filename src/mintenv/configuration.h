/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_CONFIGURATION_H
#define MINTAITRAININGENV_CONFIGURATION_H

#include "mintenv/logger.h"
#include <string>

// Macro for compacter definition of properties (to save boilerplate code)
#define PROPERTY(TYPE, PROP, GETTER) \
    private: \
        TYPE PROP ; \
    public: \
        const TYPE & GETTER () const { return PROP; }

/**
 * Configuration ist eine Hilfsklasse, die Programmeinstellungen ausliest und zur Verfügung stellt.
 * Da das Programm in einem Docker-Container genutzt werden soll, werden die Einstellungen aus den Environment-Variablen ausgelesen.
 */
class Configuration {
public:
    Configuration();

    // Prints the configuration to the standard output
    void print() const;

    // Logging
    PROPERTY( std::string, logMaxLevel,               getLogMaxLevel                 )

    // Properties for the video source
    PROPERTY( std::string, sourceType,                getSourceType                  )
    PROPERTY( std::string, sourceWebcamDeviceNoStr,   getSourceWebcamDeviceNumberStr )
    PROPERTY( std::string, sourceResolutionWidthStr,  getSourceResolutionWidthStr    )
    PROPERTY( std::string, sourceResolutionHeightStr, getSourceResolutionHeightStr   )
    PROPERTY( int,         sourceWebcamDeviceNo,      getSourceWebcamDeviceNumber    )
    PROPERTY( int,         sourceResolutionWidth,     getSourceResolutionWidth       )
    PROPERTY( int,         sourceResolutionHeight,    getSourceResolutionHeight      )

    // YOLO configuration settings
    PROPERTY( std::string, yoloConfigurationFile,     getYoloConfigurationFile       )
    PROPERTY( std::string, yoloWeightsFile,           getYoloWeightsFile             )
    PROPERTY( std::string, yoloNamesFile,             getYoloNamesFile               )

    // Object detection parameters
    PROPERTY( std::string, detectionThresholdStr,     getDetectionThresholdStr       )
    PROPERTY( std::string, detectionMaxBoxOverlapStr, getDetectionMaxBoxOverlapStr   )
    PROPERTY( std::string, detectionBlobSizeStr,      getDetectionBlobSizeStr        )
    PROPERTY( float,       detectionThreshold,        getDetectionThreshold          )
    PROPERTY( float,       detectionMaxBoxOverlap,    getDetectionMaxBoxOverlap      )
    PROPERTY( int,         detectionBlobSize,         getDetectionBlobSize           )

    // Output Video settings (stream and video)
    PROPERTY( std::string, targetResolutionWidthStr,  getTargetResolutionWidthStr    )
    PROPERTY( std::string, targetResolutionHeightStr, getTargetResolutionHeightStr   )
    PROPERTY( std::string, targetFramerateStr,        getTargetFramerateStr          )
    PROPERTY( int,         targetResolutionWidth,     getTargetResolutionWidth       )
    PROPERTY( int,         targetResolutionHeight,    getTargetResolutionHeight      )
    PROPERTY( int,         targetFramerate,           getTargetFramerate             )

    // Output stream settings
    PROPERTY( std::string, streamHost,                getStreamHost                  )
    PROPERTY( std::string, streamPort,                getStreamPort                  )
    PROPERTY( std::string, streamPath,                getStreamPath                  )
    PROPERTY( std::string, streamUser,                getStreamUser                  )
    PROPERTY( std::string, streamPassword,            getStreamPassword              )
    PROPERTY( std::string, streamUrl,                 getStreamUrl                   )

private:
    const Logger logger;

    // helper methods
    static std::string getFromEnv(const std::string &envVarName, const std::string &defaultVal);
};


#endif //MINTAITRAININGENV_CONFIGURATION_H
