/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#include "mintenv/configuration.h"
#include "mintenv/pipeline/processing_pipeline.h"
#include "mintenv/pipeline/source/generic_webcam_source.h"
#include "mintenv/pipeline/source/zed2_camera_source.h"
#include "mintenv/pipeline/transform/label_writer.h"
#include "mintenv/pipeline/transform/stream_output.h"
#include "mintenv/pipeline/transform/yolo_detector.h"

#include <iostream>
#include <csignal>

using namespace pipeline;
using namespace pipeline::source;
using namespace pipeline::transform;

ProcessingPipeline *processingPipeline = nullptr;
const Logger mainLogger("Main");

/**
 * Utility method to allow switch-case operations on strings (by assigning a constant integer to them)
 * Source: https://stackoverflow.com/a/16388610
 */
constexpr unsigned int string2int(const char *str, int h = 0) {
    return !str[h] ? 5381 : (string2int(str, h + 1) * 33) ^ str[h];
}

/**
 * Handling the received signals to properly shutdown the program
 */
void handleSignal(int signal) {
    mainLogger.warn("Signal erhalten: " + std::to_string(signal));
    processingPipeline->stop();
}


int main(int argc, char **argv) {
    mainLogger.info("########################################");
    mainLogger.info("Starte Initialisierung des Programms.");
    mainLogger.info("########################################");

    // Load settings and create configuration
    Configuration config;

    try {
        mainLogger.debug("Initialisiere Bildquelle.");
        bool useSeparateDetectionStage = true;

        // Init image source based on enabled modes and configuration
        ImageSource *src;
        switch (string2int(config.getSourceType().c_str())) {
#ifdef ZED2_CAMERA
            case string2int("ZED2"):
                // If we use ZED2, the detection is done by the camera library
                useSeparateDetectionStage = false;

                mainLogger.info("Verwende ZED2-Kamera als Bildquelle.");
                src = new Zed2CameraSource(config);
                break;
#endif
            case string2int("WEBCAM"):
            default:
                mainLogger.info("Verwende generische Kamera als Bildquelle.");
                src = new WebcamSource(config);
                break;
        }

        processingPipeline = new ProcessingPipeline(config, src);

        // Initialize pipeline and stages
        mainLogger.debug("Initialisiere Verarbeitungsschritte.");
        if (useSeparateDetectionStage) {
            processingPipeline->addPipelineStage(new YoloDetector(config));
        }
        processingPipeline->addPipelineStage(new LabelWriter(config));
        processingPipeline->addPipelineStage(new StreamOutput(config));

    } catch (std::runtime_error &err) {
        delete processingPipeline;
        mainLogger.error("Fehler bei der Initialisierung des Programms: \"" + std::string(err.what()) + "\"");
        exit(-2);
    }

    // Catch signals to allow a proper shutdown
    mainLogger.debug("Initialisiere Behandlung für UNIX-Signale.");
    std::signal(SIGINT, &handleSignal);
    std::signal(SIGTERM, &handleSignal);

    try {
        mainLogger.info("Initialisierung erfolgreich.");
        mainLogger.info("Starte die Verarbeitungspipeline.");
        processingPipeline->notifyPipelineStart();

        // run pipeline while everything is working properly
        while (processingPipeline->isRunning() && processingPipeline->runPipeline()) {
            mainLogger.trace("Pipelinecycle vollständig.");
        }

    } catch (std::exception &err) {
        delete processingPipeline;
        mainLogger.error("Fehler während der Ausführung der Pipeline: \"" + std::string(err.what()) + "\"");
        exit(-3);
    }

    mainLogger.info("Pipeline wurde beendet. Deinitialisiere Komponenten.");
    delete processingPipeline;
    mainLogger.info("Beende Programm.");
    return 0;
}
