/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#include "image.h"

#include <utility>

ImageRegion::ImageRegion(int x, int y, int width, int height) : x(x), y(y), width(width), height(height), labels() {}

int ImageRegion::getX() const {
    return x;
}

int ImageRegion::getY() const {
    return y;
}

int ImageRegion::getWidth() const {
    return width;
}

int ImageRegion::getHeight() const {
    return height;
}

void ImageRegion::addLabel(const std::string &key, const std::string &value) {
    labels.push_back(std::make_pair(key, value));
}

const ImageRegionLabels &ImageRegion::getLabels() const {
    return labels;
}

Image::Image(cv::Mat imageData) : imageData(std::move(imageData)) {}

Image::~Image() = default;

const std::vector<ImageRegion> &Image::getRegions() const {
    return regions;
}

void Image::setRegions(const std::vector<ImageRegion> &regs) {
    Image::regions = regs;
}

cv::Mat &Image::getImageData() {
    return Image::imageData;
}
