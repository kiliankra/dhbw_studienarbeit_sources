/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_LOGGER_H
#define MINTAITRAININGENV_LOGGER_H

#include <string>
#include <mutex>

enum LogLevel {
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR
};

class Logger {
public:
    explicit Logger(std::string loggerName);

    void trace(std::string line) const;
    void debug(std::string line) const;
    void info(std::string line) const;
    void warn(std::string line) const;
    void error(std::string line) const;

    void log(LogLevel level, std::string &line) const;

    static LogLevel getMaxLogLevel();

    static void setMaxLogLevel(LogLevel maxLogLevel);

private:
    static std::mutex loggerMutex;
    static LogLevel maxLogLevel;
    const std::string loggerName;
};


#endif //MINTAITRAININGENV_LOGGER_H
