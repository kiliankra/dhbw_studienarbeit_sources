/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_UTIL_H
#define MINTAITRAININGENV_UTIL_H

#include <cfloat>
#include <opencv2/opencv.hpp>
#ifdef ZED2_CAMERA
#include <sl/Camera.hpp>
#endif

#include "mintenv/configuration.h"
#include "mintenv/logger.h"

struct DetectedObject {
    cv::Rect bbox;
    int32_t classIdx;
    float confidence;
};

/**
 * Implementiert verschiedene Hilfsfunktionen für den Umgang mit OpenCV, bzw. der Objektdetektion.
 */
class DetectionUtil {
public:
    DetectionUtil(const Configuration &configuration);

    /**
     * Führt eine Bilddetektion mit OpenCV's DNN Implementierung durch.
     * Parameter, wie beispielsweise der Confidence-Threshold, werden aus der Konfiguration gelesen.
     *
     * @return Liste der detektierten Objekte
     */
     std::vector<DetectedObject> runDetection(cv::Mat &img);

    const std::vector<std::string> &getClassNames() const;

    /**
     * Entfernt den Alpha-Channel aus einer <code>cv::Mat</code>.
     * Hierfür wird die Konversion <code>cv::COLOR_BGRA2BGR</code> verwendet.
     *
     * @param src Ursprungsobjekt. Es werden 4 Farbkanälen (B,G,R,A) erwartet.
     * @return Modifizierte Matrix mit 3 Farbkanälen (B,G,R)
     */
    static cv::Mat removeAlphaChannel(const cv::Mat& src);

#ifdef ZED2_CAMERA

    /**
     * Konvertiert den Matrix-Typ der ZED-Bibliothek zu OpenCV
     */
    static int slMatType2cvMatType(sl::MAT_TYPE type);

    /**
     * Konvertiert eine Matrix aus der ZED-Bibliothek zu OpenCV
     */
    static cv::Mat slMat2cvMat(sl::Mat &input);

    /**
     * Konvertiert ein Rechteck von OpenCV zu einer Box für die ZED-Bibliothek
     */
    static std::vector<sl::uint2> cvRect2slBox(cv::Rect &input, size_t maxWidth, size_t maxHeight);

    static std::vector<sl::CustomBoxObjectData> detections2slCustomObjects(std::vector<DetectedObject> inputs, size_t maxWidth, size_t maxHeight);

    static sl::CustomBoxObjectData detection2slCustomObjectData(DetectedObject &input, size_t maxWidth, size_t maxHeight);
#endif

private:
    const Logger logger;
    const Configuration &configuration;

    std::vector<std::string> classNames;
    cv::dnn::Net detector;
    std::vector<std::string> detectorOutputLayerNames;

    void logDetection(DetectedObject obj);
};


#endif //MINTAITRAININGENV_UTIL_H
