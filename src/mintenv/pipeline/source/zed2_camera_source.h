/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_ZED2_CAMERA_SOURCE_H
#define MINTAITRAININGENV_ZED2_CAMERA_SOURCE_H
#ifdef ZED2_CAMERA

#include "image_source.h"

#include "mintenv/configuration.h"
#include "mintenv/detection_util.h"

#include <sl/Camera.hpp>

namespace pipeline { namespace source {

    class Zed2CameraSource : public ImageSource {
    public:
        explicit Zed2CameraSource(const Configuration &config);
        ~Zed2CameraSource() override;
        Image *provideImageFrame() override;

        std::string getName() const override;

    private:
        const Configuration config;
        const Logger logger;
        DetectionUtil detectionHelper;

        sl::Camera zedCamera;
        sl::Resolution cameraRes;

        void checkResult(std::string msg, sl::ERROR_CODE result);
    };

}}

#endif //ZED2_CAMERA
#endif //MINTAITRAININGENV_ZED2_CAMERA_SOURCE_H
