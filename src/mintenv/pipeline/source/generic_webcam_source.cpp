/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#include "generic_webcam_source.h"

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace pipeline::source;

WebcamSource::WebcamSource(const Configuration &config) : config(config), logger("GenericWebcam"), videoSource(),
                                                          currentFrame(), currentFrameFetched(true),
                                                          currentFrameMutex(), running(true) {
    logger.info("Initialisiere WebcamSource.");

    // Try to open the requested webcam as source
    // Note: GStreamer is NOT used as preferred API, since it is pretty buggy with the ribbon-camera (https://answers.opencv.org/question/203477/opencv_343-hangs-on-videocapture-resolved/)
    if (videoSource.open(config.getSourceWebcamDeviceNumber(), cv::CAP_V4L2)) {

        // Set resolution as provided by the configuration
        videoSource.set(cv::CAP_PROP_FRAME_HEIGHT, config.getSourceResolutionHeight());
        videoSource.set(cv::CAP_PROP_FRAME_WIDTH, config.getSourceResolutionWidth());
    } else {
        logger.info("Öffnen der Webcam fehlgeschlagen.");
        throw std::runtime_error("Initialization failed");
    }

    // start reading the images from the webcam
    imgReadThread = std::thread(&WebcamSource::readImageFrames, this);
}

WebcamSource::~WebcamSource() {
    // Stop reading from webcam
    logger.info("Stoppe Auslesen der Webcam.");
    running = false;
    if (imgReadThread.joinable())
        imgReadThread.join();
}

Image *WebcamSource::provideImageFrame() {
    cv::Mat copiedFrame;
    bool frameProvided = false;
    logger.trace("Stelle aktuell gecachten Frame zur Verfügung.");

    // Ensure a new frame is provided every time
    while (!frameProvided) {
        currentFrameMutex.lock();
        if (!currentFrameFetched) {
            currentFrame.copyTo(copiedFrame);
            currentFrameFetched = true;
            frameProvided = true;
        }
        currentFrameMutex.unlock();

        if (!frameProvided) {
            // Retry after 10 ms
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }

    // Return image without additional labels
    return new Image(copiedFrame);
}

void WebcamSource::readImageFrames() {
    logger.debug("Starte Auslesen des Webcam-Videos.");
    try {
        while (running) {
            logger.trace("Neues Bild gelesen.");
            cv::Mat readFrame;
            videoSource.read(readFrame);
            updateFrame(readFrame);
        }
    } catch (std::runtime_error &ex) {
        logger.error("Ein Fehler ist während des Auslesens der Webcam aufgetreten: " + std::string(ex.what()));
    }
}

void WebcamSource::updateFrame(cv::Mat &frame) {
    logger.trace("Aktualisiere gecachten Frame.");
    currentFrameMutex.lock();
    frame.copyTo(currentFrame);
    currentFrameFetched = false;
    currentFrameMutex.unlock();
}

std::string WebcamSource::getName() const {
    return "(Generische) Kamera Videoquelle";
}
