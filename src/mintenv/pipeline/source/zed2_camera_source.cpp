/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifdef ZED2_CAMERA

#include "zed2_camera_source.h"

using namespace sl;

pipeline::source::Zed2CameraSource::Zed2CameraSource(const Configuration &config) :
        config(config), detectionHelper(config), zedCamera(), logger("ZedCameraSource") {
    logger.info("Initialisiere Zed2CameraSource");

    InitParameters initParams;
    if (config.getSourceResolutionWidth() == 1920 && config.getSourceResolutionHeight() == 1080) {
        initParams.camera_resolution = sl::RESOLUTION::HD1080;
    } else if (config.getSourceResolutionWidth() == 1280 && config.getSourceResolutionHeight() == 720) {
        initParams.camera_resolution = sl::RESOLUTION::HD720;
    } else {
        throw std::runtime_error("Ungültige Quellauflösung. In diesem Kamera-Modus werden die folgenden Auflösungen unterstützt: 1280x720, 1920x1080");
    }
    initParams.camera_fps = config.getTargetFramerate();
    initParams.depth_mode = sl::DEPTH_MODE::PERFORMANCE;
    initParams.coordinate_units = sl::UNIT::CENTIMETER;
    initParams.coordinate_system = sl::COORDINATE_SYSTEM::RIGHT_HANDED_Y_UP;

    auto success = zedCamera.open(initParams);
    checkResult("Initialisierung", success);

    logger.info("Verbindung zur ZED2-Kamera hergestellt. Seriennummer: " +
                std::to_string(zedCamera.getCameraInformation().serial_number));

    success = zedCamera.enablePositionalTracking();
    checkResult("Aktiviere Positionstracking", success);

    sl::ObjectDetectionParameters objectDetectionParameters;
    objectDetectionParameters.enable_tracking = true;
    objectDetectionParameters.detection_model = sl::DETECTION_MODEL::CUSTOM_BOX_OBJECTS;
    success = zedCamera.enableObjectDetection(objectDetectionParameters);
    checkResult("Aktiviere Positionstracking", success);

    cameraRes = zedCamera.getCameraInformation().camera_configuration.resolution;
}

pipeline::source::Zed2CameraSource::~Zed2CameraSource() {
    zedCamera.close();
}

Image *pipeline::source::Zed2CameraSource::provideImageFrame() {
    // Grab data from ZED2
    auto result = zedCamera.grab();
    checkResult("Aktualisiere Daten", result);

    Mat img(cameraRes.width / 2, cameraRes.height / 2, MAT_TYPE::U8_C4);
    result = zedCamera.retrieveImage(img);
    checkResult("Lese Bilddaten", result);

    cv::Mat openCvImage = DetectionUtil::slMat2cvMat(img);
    cv::Mat brgImage = DetectionUtil::removeAlphaChannel(openCvImage);

    // Run detection on the image
    auto detectedObjs = detectionHelper.runDetection(brgImage);
    auto slDetectedObjs = DetectionUtil::detections2slCustomObjects(detectedObjs, cameraRes.width, cameraRes.height);
    zedCamera.ingestCustomBoxObjects(slDetectedObjs);

    Objects outputObjects;
    result = zedCamera.retrieveObjects(outputObjects);
    checkResult("Lese Objektdaten", result);

    auto toReturn = new Image(brgImage);
    std::vector<ImageRegion> regions;

    for (const auto &obj: outputObjects.object_list) {
        auto topLeft = obj.bounding_box_2d[0];
        auto botRight = obj.bounding_box_2d[2];
        ImageRegion region(static_cast<int32_t>(topLeft.x),
                           static_cast<int32_t>(topLeft.y),
                           static_cast<int32_t>(botRight.x - topLeft.x),
                           static_cast<int32_t>(botRight.y - topLeft.y));

        std::stringstream dimensionsStr;
        dimensionsStr << obj.dimensions.tx << "x"
                      << obj.dimensions.ty << "x"
                      << obj.dimensions.tz;
        int32_t distance = obj.position.size();

        region.addLabel("Obj.", detectionHelper.getClassNames()[obj.raw_label]);
        region.addLabel("Prob.", std::to_string(obj.confidence));
        region.addLabel("Dim.", dimensionsStr.str());
        region.addLabel("Dist.", std::to_string(distance));
        regions.push_back(region);
    }

    toReturn->setRegions(regions);
    return toReturn;
}

void pipeline::source::Zed2CameraSource::checkResult(std::string msg, sl::ERROR_CODE result) {
    if (result != sl::ERROR_CODE::SUCCESS) {
        std::stringstream logMsg;
        logMsg << "Fehlercode \"" << toString(result)
               << "\" bei \"" << msg << "\""
               << " von der ZED2-Kamera erhalten.";
        logger.error(logMsg.str());
        throw std::runtime_error(toString(result));
    }
}

std::string pipeline::source::Zed2CameraSource::getName() const {
    return "ZED2 Videoquelle";
}

#endif //ZED2_CAMERA
