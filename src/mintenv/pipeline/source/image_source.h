/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_IMAGE_SOURCE_H
#define MINTAITRAININGENV_IMAGE_SOURCE_H

#include "mintenv/image.h"

namespace pipeline { namespace source {

    /**
     * Interface für Klassen, die als Quelle für Bilder genutzt werden können
     */
    class ImageSource {
    public:
        virtual ~ImageSource() = default;

        /**
         * Stellt ein aktuelles Bild aus der Quelle zur Weiterverarbeitung innerhalb der Pipeline bereit.
         *
         * @return Aktuelles Bild das aus der Quelle gelesen wurde
         */
        virtual Image *provideImageFrame() = 0;

        /**
         * Gibt einen Namen für diese Bildquelle zurück.
         *
         * @return Name der Implementierung oder des Objekts.
         */
        virtual std::string getName() const = 0;
    };

}}

#endif //MINTAITRAININGENV_IMAGE_SOURCE_H
