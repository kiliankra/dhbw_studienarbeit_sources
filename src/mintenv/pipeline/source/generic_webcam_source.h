/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_WEBCAM_SOURCE_H
#define MINTAITRAININGENV_WEBCAM_SOURCE_H

#include "image_source.h"

#include "mintenv/configuration.h"

#include <chrono>
#include <mutex>
#include <thread>

namespace pipeline { namespace source {

    class WebcamSource : public ImageSource {
    public:
        explicit WebcamSource(const Configuration &config);
        ~WebcamSource() override;

        Image *provideImageFrame() override;
        void readImageFrames();

        std::string getName() const override;

    private:
        const Configuration &config;
        const Logger logger;

        cv::VideoCapture videoSource;
        cv::Mat currentFrame;
        bool currentFrameFetched;

        std::thread imgReadThread;
        std::mutex currentFrameMutex;
        bool running;

        void updateFrame(cv::Mat &frame);
    };

}}

#endif //MINTAITRAININGENV_WEBCAM_SOURCE_H
