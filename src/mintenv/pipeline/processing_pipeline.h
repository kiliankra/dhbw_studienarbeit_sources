/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_PROCESSING_PIPELINE_H
#define MINTAITRAININGENV_PROCESSING_PIPELINE_H

#include "mintenv/configuration.h"
#include "mintenv/logger.h"
#include "mintenv/pipeline/source/image_source.h"
#include "mintenv/pipeline/transform/image_transformer.h"
#include <vector>

namespace pipeline {

    class ProcessingPipeline {
    public:
        ProcessingPipeline(const Configuration &config, source::ImageSource *imgSrc);
        ~ProcessingPipeline();

        void addPipelineStage(transform::ImageTransformer *transformer);

        void notifyPipelineStart();
        bool runPipeline();

        bool isRunning() const;
        void stop();

    private:
        const Logger logger;
        bool running = true;

        source::ImageSource *src;
        std::vector<transform::ImageTransformer *> stages;
    };

}

#endif //MINTAITRAININGENV_PROCESSING_PIPELINE_H
