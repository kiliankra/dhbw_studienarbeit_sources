/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_IMAGE_TRANSFORMER_H
#define MINTAITRAININGENV_IMAGE_TRANSFORMER_H

#include "mintenv/image.h"

namespace pipeline { namespace transform {

    class ImageTransformer {
    public:
        /**
         * Virtueller Destruktor zur Vermeidung von Memory-Leaks.
         */
        virtual ~ImageTransformer() = default;

        /**
         * Wendet diesen Verarbeitungsschritt auf das übergebene Bild an.
         *
         * @param image Bild zur Verarbeitung
         * @return Ob die Verarbeitung erfolgreich war.
         */
        virtual bool transformImage(Image *image) = 0;

        /**
         * Gibt einen Namen für diesen Verarbeitungsschritt zurück.
         *
         * @return Name der Implementierung oder des Objekts.
         */
        virtual std::string getName() const = 0;
    };

}}

#endif //MINTAITRAININGENV_IMAGE_TRANSFORMER_H
