/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#include "yolo_detector.h"

#include <fstream>

using namespace pipeline::transform;

YoloDetector::YoloDetector(const Configuration &configuration) : logger("YoloDetector"),
                                                                 detectionHelper(configuration) {
    logger.info("Initialisiere YoloDetector.");
}

YoloDetector::~YoloDetector() = default;

std::string getRoundedPercentage(float probability) {
    // Round to one digit
    auto rounded = std::round(probability * 1000) / 10;
    return std::to_string(rounded) + "%";
}

bool YoloDetector::transformImage(Image *image) {
    auto detected = detectionHelper.runDetection(image->getImageData());

    std::vector<ImageRegion> detectedRegions;
    for (const auto &item: detected) {
        ImageRegion region(item.bbox.x, item.bbox.y,
                           item.bbox.width, item.bbox.height);

        region.addLabel("Obj.", detectionHelper.getClassNames()[item.classIdx]);
        region.addLabel("Prob.", getRoundedPercentage(item.confidence));
        detectedRegions.push_back(region);
    }

    logger.debug(std::to_string(detectedRegions.size()) + " Objekte erkannt.");
    image->setRegions(detectedRegions);
    return true;
}

std::string YoloDetector::getName() const {
    return "Yolo-Detektion";
}
