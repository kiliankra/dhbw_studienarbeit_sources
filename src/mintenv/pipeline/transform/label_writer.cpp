/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#include "label_writer.h"

#include <iostream>
#include <sstream>

#include "opencv2/opencv.hpp"

using namespace pipeline::transform;

// Define the color constants
const cv::Scalar LabelWriter::GREEN = cv::Scalar_<double>(145.0, 207.0, 31.0);
const cv::Scalar LabelWriter::WHITE = cv::Scalar_<double>(255.0, 255.0, 255.0);

LabelWriter::LabelWriter(const Configuration &configuration) : config(configuration), logger("LabelWriter") {
    logger.info("Initialisiere LabelWriter");
}

LabelWriter::~LabelWriter() = default;

bool LabelWriter::transformImage(Image *image) {
    // Get a reference to the Image data, which can be manipulated
    auto img = image->getImageData();

    // Draw all regions in the image
    for (const auto &region: image->getRegions()) {
        // Build text to display for this region
        std::stringstream displayText;
        bool first = true;
        for (const auto &item: region.getLabels()) {
            if (first) {
                first = false;
            } else {
                displayText << ", ";
            }

            displayText << item.first << ": " << item.second;
        }
        logger.trace("Schreibe \"" + displayText.str() + "\".");

        // Draw box around the region
        auto topLeft = cv::Point_<int>(region.getX(), region.getY());
        auto bottomRight = cv::Point_<int>(region.getX() + region.getWidth(), region.getY() + region.getHeight());
        cv::rectangle(img, topLeft, bottomRight, GREEN, 3);

        // Draw the label/Display for the region
        drawTextWithBackground(img, region.getX() + 10, region.getY() + 10,
                               displayText.str(), WHITE, GREEN,
                               3.0 * (static_cast<float>(img.rows) / 1000.0));
    }

    return true;
}

std::string LabelWriter::getName() const {
    return "Beschriftungsschreiber";
}

// Copied and modified from https://stackoverflow.com/a/65146731
void LabelWriter::drawTextWithBackground(
        cv::Mat &img, int x, int y, const std::string &text, const cv::Scalar &textColor,
        const cv::Scalar &backgroundColor, double fontScale, int font, int thickness) {
    // Get size of the text when it's drawn
    auto textSize = cv::getTextSize(text, font, fontScale, thickness, nullptr);

    // Draw rectangle as background
    cv::rectangle(img,
                  cv::Point_<int>(x, y),
                  cv::Point_<int>(x + textSize.width, y + textSize.height),
                  backgroundColor, cv::FILLED);

    // Put Text at an appropriate position
    cv::putText(img, text,
                cv::Point_<int>(x, y + textSize.height),
                font, fontScale, textColor);
}
