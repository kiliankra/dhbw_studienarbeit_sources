/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_YOLO_DETECTOR_H
#define MINTAITRAININGENV_YOLO_DETECTOR_H

#include "image_transformer.h"

#include "mintenv/configuration.h"
#include "mintenv/detection_util.h"
#include "mintenv/logger.h"

#include <opencv2/opencv.hpp>

namespace pipeline { namespace transform {

    class YoloDetector : public ImageTransformer {
    public:
        explicit YoloDetector(const Configuration &configuration);
        ~YoloDetector() override;

        bool transformImage(Image *image) override;
        std::string getName() const override;

    private:
        const Logger logger;
        DetectionUtil detectionHelper;
    };

}}

#endif //MINTAITRAININGENV_YOLO_DETECTOR_H
