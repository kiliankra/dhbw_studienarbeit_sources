/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#include "stream_output.h"

#include <iostream>
#include <csignal>
#include "opencv2/videoio.hpp"

using namespace pipeline::transform;

StreamOutput::~StreamOutput() {
    logger.info("Stoppe Streaming-Ausgabe.");
    running = false;
    if (streamWriteThread.joinable())
        streamWriteThread.join();
}

bool StreamOutput::transformImage(Image *image) {
    currentFrameMutex.lock();
    logger.trace("Aktualisiere Frame zum Schreiben in den Stream.");
    currentFrame = image->getImageData().clone();

    if (!streamWriter.isOpened()) {
        initializeStream();
    }

    currentFrameMutex.unlock();
    return true;
}

StreamOutput::StreamOutput(const Configuration &configuration) :
        logger("StreamOutput"), config(configuration),
        streamWriter(), running(false),
        frameTimeMs(std::chrono::milliseconds(1000 / config.getTargetFramerate())) {
    logger.info("Initialisiere Stream-Ausgabe.");
}

void StreamOutput::writeStream() {
    try {
        while (running) {
            logger.trace("Schreibe Frame in Stream.");
            auto nextExecution = std::chrono::steady_clock::now() + frameTimeMs;
            currentFrameMutex.lock();
            streamWriter << currentFrame;
            currentFrameMutex.unlock();
            std::this_thread::sleep_until(nextExecution);
        }
    } catch (std::runtime_error &ex) {
        logger.error("Ein Fehler ist beim Schreiben zum Stream aufgetreten: " + std::string(ex.what()));
        logger.error("Beende Programm durch ein SIGINT.");
        raise(SIGINT); // Terminate program
    }
}

void StreamOutput::initializeStream() {
    std::stringstream gstreamerCmd;
    // Define rtsp target
    gstreamerCmd << "rtspclientsink name=rtsp protocols=tcp latency=0 location=" << config.getStreamUrl() << "    ";

    // Build the command
    gstreamerCmd
            << "appsrc is-live=true ! "
            << "autovideoconvert ! "
            << "videoscale ! "
            << "video/x-raw,format=I420,"
            << "width=" << config.getTargetResolutionWidthStr() << ","
            << "height=" << config.getTargetResolutionHeightStr() << " ! "
            << "jpegenc ! "
            << "rtsp.sink_0 ";

    logger.info("Starte GStreamer mit der Konfiguration \"" + gstreamerCmd.str() + "\"");

    auto size = cv::Size_<int>(config.getSourceResolutionWidth(), config.getSourceResolutionHeight());
    int fourcc = cv::VideoWriter::fourcc('M', 'J', 'P', 'G');


    streamWriter.open(gstreamerCmd.str(),
                      fourcc,
                      config.getTargetFramerate(),
                      size);

    // Start writing frames to the stream
    running = true;
    streamWriteThread = std::thread(&StreamOutput::writeStream, this);
}

std::string StreamOutput::getName() const {
    return "Stream-Ausgabe " + config.getStreamUrl();
}
