/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_LABEL_WRITER_H
#define MINTAITRAININGENV_LABEL_WRITER_H

#include "image_transformer.h"

#include "mintenv/configuration.h"
#include "mintenv/logger.h"

namespace pipeline {
    namespace transform {

        class LabelWriter : public ImageTransformer {
        public:
            explicit LabelWriter(const Configuration &configuration);

            ~LabelWriter() override;

            bool transformImage(Image *image) override;

            std::string getName() const override;

        private:
            // Constants for used colors
            const static cv::Scalar GREEN;
            const static cv::Scalar WHITE;

            const Configuration &config;
            const Logger logger;

            static void drawTextWithBackground(
                    cv::Mat &img, int x, int y, const std::string &text,
                    const cv::Scalar &textColor, const cv::Scalar &backgroundColor,
                    double fontScale = 3.0, int font = cv::FONT_HERSHEY_PLAIN, int thickness = 1);
        };

    }
}

#endif //MINTAITRAININGENV_LABEL_WRITER_H
