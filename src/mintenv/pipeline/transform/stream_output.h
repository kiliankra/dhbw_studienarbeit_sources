/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_STREAM_OUTPUT_H
#define MINTAITRAININGENV_STREAM_OUTPUT_H

#include "image_transformer.h"

#include "mintenv/configuration.h"
#include "mintenv/logger.h"

#include <chrono>
#include <mutex>
#include <thread>

namespace pipeline { namespace transform {

    class StreamOutput : public ImageTransformer {
    public:
        explicit StreamOutput(const Configuration &configuration);
        ~StreamOutput() override;

        bool transformImage(Image *image) override;
        void writeStream();

        std::string getName() const override;

    private:
        const Configuration &config;
        const Logger logger;

        cv::VideoWriter streamWriter;

        cv::Mat currentFrame;

        std::thread streamWriteThread;
        std::mutex currentFrameMutex;
        bool running;
        std::chrono::milliseconds frameTimeMs;

        void initializeStream();
    };

}}

#endif //MINTAITRAININGENV_STREAM_OUTPUT_H
