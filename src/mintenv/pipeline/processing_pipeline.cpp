/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#include "processing_pipeline.h"

#include <mintenv/configuration.h>

using namespace pipeline;
using namespace pipeline::source;
using namespace pipeline::transform;

ProcessingPipeline::ProcessingPipeline(const Configuration &config, source::ImageSource *imgSrc)
        : logger("Pipeline"), src(imgSrc), stages() {
    logger.info("Initialisiere Pipeline mit Bildquelle \"" + src->getName() + "\".");
}

ProcessingPipeline::~ProcessingPipeline() {
    delete src;
    for (auto &item: stages)
        delete item;
}

void ProcessingPipeline::addPipelineStage(transform::ImageTransformer *transformer) {
    logger.debug("Füge Verarbeitungsschritt \"" + transformer->getName() + "\" hinzu.");
    stages.emplace_back(transformer);
}

void ProcessingPipeline::notifyPipelineStart() {
    std::stringstream logString;
    logString << "Starte Pipeline:\n"
              << src->getName();
    for (const auto &item : stages) {
        logString << " -> " << item->getName();
    }
    logger.info(logString.str());
}

bool ProcessingPipeline::runPipeline() {
    Image *img = src->provideImageFrame();
    logger.trace("Bild gelesen.");

    for (auto &item: stages) {
        logger.trace("Starte Verarbeitungsschritt \"" + item->getName() + "\".");
        auto res = item->transformImage(img);
        logger.trace("Verarbeitungsschritt \"" + item->getName() + "\" beendet.");

        // Abort pipeline if stage fails
        if (!res) {
            logger.warn("Verarbeitungsschritt \"" + item->getName() + "\" fehlgeschlagen.");
            // Cleanup image memory
            delete img;
            return false;
        }
    }

    // Cleanup image memory
    delete img;
    return true;
}

bool ProcessingPipeline::isRunning() const {
    return running;
}

void ProcessingPipeline::stop() {
    logger.info("Stoppe Pipeline");
    ProcessingPipeline::running = false;
}
