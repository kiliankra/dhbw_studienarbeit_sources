/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#include "detection_util.h"

#include <fstream>
#include <sstream>

DetectionUtil::DetectionUtil(const Configuration &configuration) : configuration(configuration), classNames(),
                                                                   logger("DetectionUtil") {
    logger.info("Initialisiere DetectionUtil");

    detector = cv::dnn::readNetFromDarknet(
            configuration.getYoloConfigurationFile(),
            configuration.getYoloWeightsFile());
#ifdef CUDA
    logger.debug("Nutze CUDA-Backend für den Detektor.");
    detector.setPreferableBackend(cv::dnn::DNN_BACKEND_CUDA);
    detector.setPreferableTarget(cv::dnn::DNN_TARGET_CUDA);
#endif

    detectorOutputLayerNames = detector.getUnconnectedOutLayersNames();

    // Read names from file
    std::ifstream namesFile(configuration.getYoloNamesFile());
    std::string name;
    while (std::getline(namesFile, name)) {
        classNames.emplace_back(name);
    }
    namesFile.close();

    std::stringstream logLine;
    logLine << "Verfügbare Klassen: [";
    for (const auto &item: classNames) {
        if (item != classNames.front())
            logLine << ", ";
        logLine << item;
    }
    logLine << "]";
    logger.debug(logLine.str());
}

void DetectionUtil::logDetection(DetectedObject obj) {
    std::string clsName = getClassNames()[obj.classIdx];
    std::stringstream box;
    box << "Box (x=" << obj.bbox.x << ", y=" << obj.bbox.y << ", "
        << "size=" << obj.bbox.width << "x" << obj.bbox.height << ")";

    std::stringstream logLine;
    logLine << "Objekt erkannt: "
            << "\"" << clsName << "\""
            << " (Wahrscheinlichkeit: " << std::to_string(obj.confidence) << "; "
            << "Koordinaten: " << box.str() << ")";
    logger.trace(logLine.str());
}

/**
 * Vorgehen zum Auslesen der OpenCV-Ausgabe:
 * Orientiert an: https://www.pyimagesearch.com/2018/11/12/yolo-object-detection-with-opencv/ https://gist.github.com/YashasSamaga/e2b19a6807a13046e399f4bc3cca3a49 https://opencv-tutorial.readthedocs.io/en/latest/yolo/yolo.html
 *
 * Aufbau der detections-Ausgabe:
 * Liste von Matrizen (Jede Matrix ist die Ausgabe eines YOLO-Layers im Neuronalen Netzwerk. (z.B. 3 Layer für den verwendeten Standard "tiny-yolo-3l")
 *
 * Die Matrizen enthalten die Boxen für die detektierten Objekte.
 * Die Boxen sind zudem mit Wahrscheinlichkeiten für jede Klasse versehen.
 * Jede Box ist eine Datenreihe mit dem folgenden Aufbau:
 *
 * <X-Position (Mitte)> <Y-Position (Mitte)> <Breite> <Höhe> <Box-Confidence> <Class-Confidence 1> <Class-Confidence 2>...
 *
 * Die Position gibt die Mitte der Box an.
 * Die "Box-Confidence ist für diesen Anwendungsfall nicht relevant und nur die "Class-Confidence"-Werte werden beachtet.
 * Zunächst werden alle Kombinationen aus Box und Klasse mit einer Confidence von >20% in Betracht gezogen.
 * Nachdem alle Boxen+Klassen für diese Bedingung gefunden wurden, werden die Boxen durch `cv::dnn::NMSBoxes` reduziert, damit diese noch vernünftig angezeigt werden können.
 */
std::vector<DetectedObject> DetectionUtil::runDetection(cv::Mat &img) {
    cv::Mat blob;
    std::vector<cv::Mat> detections;

    cv::dnn::blobFromImage(img, blob,
                           (1.0 / 255.0), // Scale to 256 color range
                           cv::Size(configuration.getDetectionBlobSize(),
                                    configuration.getDetectionBlobSize()),
                           cv::Scalar(),
                           false
    );

    logger.trace("Starte Bilddetektion...");
    detector.setInput(blob);
    detector.forward(detections, detectorOutputLayerNames);
    logger.trace("Bilddetektion erfolgreich!");

    std::vector<int32_t> acceptedBoxIndizes;
    std::vector<cv::Rect> boxes;
    std::vector<int32_t> classIndizes;
    std::vector<float> scores;

    for (auto &layerOutput: detections) {
        const auto numBoxes = layerOutput.rows;
        for (int i = 0; i < numBoxes; ++i) {
            auto xPos = layerOutput.at<float>(i, 0) * static_cast<float>(img.cols);
            auto yPos = layerOutput.at<float>(i, 1) * static_cast<float>(img.rows);
            auto width = layerOutput.at<float>(i, 2) * static_cast<float>(img.cols);
            auto height = layerOutput.at<float>(i, 3) * static_cast<float>(img.rows);
            auto region = cv::Rect(static_cast<int32_t>(xPos - width / 2),
                                   static_cast<int32_t>(yPos - height / 2),
                                   static_cast<int32_t>(width),
                                   static_cast<int32_t>(height));

            // First class probability is at index 5
            for (int c = 0; c < classNames.size(); ++c) {
                auto confidence = *layerOutput.ptr<float>(i, 5 + c);
                if (confidence >= configuration.getDetectionThreshold()) {
                    boxes.push_back(region);
                    classIndizes.push_back(c);
                    scores.push_back(confidence);
                }
            }
        }
    }

    cv::dnn::NMSBoxes(boxes, scores, 0.0, configuration.getDetectionMaxBoxOverlap(), acceptedBoxIndizes);

    std::vector<DetectedObject> detectedObjects;
    for (const auto &item: acceptedBoxIndizes) {
        auto obj = DetectedObject{
                boxes[item],
                classIndizes[item],
                scores[item]
        };

        logDetection(obj);
        detectedObjects.push_back(obj);
    }

    return detectedObjects;
}

const std::vector<std::string> &DetectionUtil::getClassNames() const {
    return classNames;
}

cv::Mat DetectionUtil::removeAlphaChannel(const cv::Mat &src) {
    cv::Mat out;
    cv::cvtColor(src, out, cv::COLOR_BGRA2BGR, 3);
    return out;
}

#ifdef ZED2_CAMERA

// Implementierung aus den ZED2-Tutorials
int DetectionUtil::slMatType2cvMatType(sl::MAT_TYPE type) {
    int cv_type = -1;
    switch (type) {
        case sl::MAT_TYPE::F32_C1:
            cv_type = CV_32FC1;
            break;
        case sl::MAT_TYPE::F32_C2:
            cv_type = CV_32FC2;
            break;
        case sl::MAT_TYPE::F32_C3:
            cv_type = CV_32FC3;
            break;
        case sl::MAT_TYPE::F32_C4:
            cv_type = CV_32FC4;
            break;
        case sl::MAT_TYPE::U8_C1:
            cv_type = CV_8UC1;
            break;
        case sl::MAT_TYPE::U8_C2:
            cv_type = CV_8UC2;
            break;
        case sl::MAT_TYPE::U8_C3:
            cv_type = CV_8UC3;
            break;
        case sl::MAT_TYPE::U8_C4:
            cv_type = CV_8UC4;
            break;
        default:
            break;
    }
    return cv_type;
}

// Implementierung aus den ZED2-Tutorials
cv::Mat DetectionUtil::slMat2cvMat(sl::Mat &input) {
    // Since cv::Mat data requires a uchar* pointer, we get the uchar1 pointer from sl::Mat (getPtr<T>())
    // cv::Mat and sl::Mat will share a single memory structure
    return {
            static_cast<int>(input.getHeight()), static_cast<int>(input.getWidth()),
            slMatType2cvMatType(input.getDataType()),
            input.getPtr<sl::uchar1>(sl::MEM::CPU),
            input.getStepBytes(sl::MEM::CPU)
    };
}

// Implementierung aus den ZED2-Tutorials
std::vector<sl::uint2> DetectionUtil::cvRect2slBox(cv::Rect &input, size_t maxWidth, size_t maxHeight) {
    std::vector<sl::uint2> bbox_out(4);
    size_t leftX = input.x < 0 ? 0 : input.x;
    size_t rightX = (input.x + input.width) > maxWidth ? maxWidth : (input.x + input.width);
    size_t upY = input.y < 0 ? 0 : input.y;
    size_t botY = (input.y + input.height) > maxHeight ? maxHeight : (input.y + input.height);

    bbox_out[0] = sl::uint2(leftX, upY);
    bbox_out[1] = sl::uint2(rightX, upY);
    bbox_out[2] = sl::uint2(rightX, botY);
    bbox_out[3] = sl::uint2(leftX, botY);
    return bbox_out;
}

std::vector<sl::CustomBoxObjectData> DetectionUtil::detections2slCustomObjects(std::vector<DetectedObject> inputs, size_t maxWidth, size_t maxHeight) {
    std::vector<sl::CustomBoxObjectData> toRet;
    for (auto &item : inputs){
        toRet.push_back(detection2slCustomObjectData(item, maxWidth, maxHeight));
    }
    return toRet;
}

sl::CustomBoxObjectData DetectionUtil::detection2slCustomObjectData(DetectedObject &input, size_t maxWidth, size_t maxHeight) {
    sl::CustomBoxObjectData out;
    out.unique_object_id = sl::generate_unique_id();
    out.bounding_box_2d = cvRect2slBox(input.bbox, maxWidth, maxHeight);
    out.probability = input.confidence;
    out.label = input.classIdx;
    return out;
}

#endif
