/**************************************************************************************************
 * Dieses Projekt wurde im Rahmen einer Studienarbeit an der DHBW Mosbach entwickelt.             *
 * Jahr: 2021/2022                                                                                *
 * Entwickelt von: Kilian Krampf                                                                  *
 **************************************************************************************************/

#ifndef MINTAITRAININGENV_Image_H
#define MINTAITRAININGENV_Image_H

#include "opencv2/opencv.hpp"

#include <string>
#include <map>
#include <vector>

typedef std::vector<std::pair<std::string, std::string>> ImageRegionLabels;

class ImageRegion {
public:
    ImageRegion(int x, int y, int width, int height);

    int getX() const;
    int getY() const;

    int getWidth() const;
    int getHeight() const;

    void addLabel(const std::string &key, const std::string &value);
    const ImageRegionLabels &getLabels() const;

private:
    int x, y;
    int width, height;
    ImageRegionLabels labels;
};


class Image {
public:
    explicit Image(cv::Mat imageData);
    virtual ~Image();

    cv::Mat &getImageData();

    const std::vector<ImageRegion> &getRegions() const;
    void setRegions(const std::vector<ImageRegion> &regs);

private:
    cv::Mat imageData;
    std::vector<ImageRegion> regions;
};


#endif //MINTAITRAININGENV_Image_H
