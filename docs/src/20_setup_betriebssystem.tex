\chapter{Initiale Einrichtung des Betriebssystems}\label{ch:os}

In diesem Kapitel werden die Schritte beschrieben, die nötig sind um das Betriebssystem des Raspberry Pi, beziehungsweise des Jetson Nano, in einen einsatzbereiten Zustand zu bringen.


\section{Raspberry Pi}\label{sec:os-rpi}

Dieser Abschnitt beschreibt die Schritte, die zum Setup einer frischen Instanz des Betriebssystems durchgeführt werden müssen.
Dabei wird der Hardwareaufbau wie er im Abschnitt~\ref{subsec:hardware-raspi-setup} beschrieben ist vorausgesetzt.

\subsection{Flashen des Images auf eine MicroSD-Karte}\label{subsec:os-rpi-flash}

Um das Betriebssystem auf die MicroSD-Karte zu schreiben wird der Raspberry Pi Imager verwendet.
Dieser kann von der Raspberry Pi Homepage unter \url{https://www.raspberrypi.com/software} heruntergeladen werden.

Als Betriebssystem ist \glqq{}Raspberry Pi OS Lite (32-Bit)\grqq{} auszuwählen.
Die Funktionalität wurde mit Versionen getestet, die auf der \glqq{}Bullseye\grqq{}-Version von Debian basieren.
Die genaue Version des Images kann von \url{https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2022-01-28/} heruntergeladen werden, aber auch andere Images mit der Version \glqq{}Bullseye\grqq{} sollten kompatibel sein.

Je nach der verwendeten Version des Imagers muss gegebenenfalls ein Benutzername und ein Passwort für den Standardbenutzer angegeben werden.
Hier ist der Benutzer \glqq{}pi\grqq{} mit einem beliebigen Passwort zu wählen.

\subsection{Konfiguration des Betriebssystems}\label{subsec:os-rpi-config}
Beim ersten Boot mit der beschriebenen MicroSD-Karte wird das Dateisystem auf die Größe der gesamten Karte erweitert und anschließend ein Neustart durchgeführt.
Bei diesem Vorgang darf die Stromversorung nicht unterbrochen werden, da es ansonsten zu einem beschädigten Dateisystem kommt.

Beim zweiten Start erscheint nach einiger Zeit die Loginmaske.
Hier ist nun die Logindaten einzugeben, die beim Flashen des Images gewählt wurden.
Die Standardwerte sind der Benutzer \glqq{}pi\grqq{} und das Passwort \glqq{}raspberry\grqq{}.

Hierbei ist darauf zu achten, dass noch ein englisches Tastaturlayout konfiguriert ist.
In Anhang~\ref{ch:appendix-keyboard} befindet sich eine Liste von ausgewählten Unterschieden zwischen deutschem und englischen Tastaturlayout.

Für die Konfiguration des Systems existiert ein automatisiertes Skript.
Die Verwendung dieses Skripts wird in Abschnitt~\ref{subsubsec:os-rpi-config-script} beschrieben.
Sollte die Verwendung eines Skripts nicht gewünscht sein, sind in Abschnitt~\ref{subsubsec:os-rpi-config-script} alle nötigen Schritte auch noch einmal manuell beschrieben.

\subsubsection{Nutzung des automatisierten Skripts}\label{subsubsec:os-rpi-config-script}
Dieser Abschnitt beschreibt zunächst wie das Setup-Skript auf dem Gerät verfügbar gemacht werden kann.
Anschließend wird die Ausführung des Skripts, sowie mögliche Probleme mit Lösungen beschrieben.

Zunächst ist der vollständige Inhalt des \texttt{setup}-Ordners aus dem Git-Repository des Projektes herunterzuladen und auf einem USB-Stick mit FAT32-Dateisystem abgelegt werden.
Die Inhalte des Verzeichnisses können unter folgendem Link heruntergeladen werden: \url{https://gitlab.com/kiliankra/dhbw_studienarbeit_sources/-/archive/main/dhbw_studienarbeit_sources-main.zip?path=setup}.
Diese ZIP-Datei sollte entpackt werden bevor die Inhalte auf den USB-Stick übertragen werden.

Nach dem Start des Systems, wie er im Abschnitt~\ref{subsec:os-rpi-config} beschrieben ist, kann nun der USB-Stick an das Gerät angeschlossen werden.
Dem USB-Stick wird vom Betriebssystem eine Datei-Referenz zugewiesen.
Im beschriebenen Zustand ist diese Referenz \texttt{/dev/sda}, beziehungsweise \texttt{/dev/sda1} für die Partition mit dem Dateisystem.
Um auf das Dateisystem zugreifen zu können, muss es in die Dateistruktur eingehängt werden.
Dies erfolgt über den Befehl \texttt{sudo mount /dev/sda1 /mnt}.
Hierbei ist auf das englische Tastaturlayout zu achten, das noch eingestellt ist.
Eine Hilfestellung hierzu findet sich im Anhang~\ref{ch:appendix-keyboard}.

Nun kann mit dem Befehl \texttt{cd /mnt} in das Verzeichnis des USB-Sticks gewechselt werden.
Dort liegt das Setup-Skript unter dem Namen \texttt{setup\_raspberry.sh}.
Das Skript hat zwei \textbf{optionale} Aufrufparameter: Die SSID und das Passwort des WLAN-Zugangs der automatisch konfiguriert werden soll.
Eine WLAN-Verbindung ist zur Ausführung des Setups nicht zwingend nötig, jedoch wird eine Verbindung zum Internet vorausgesetzt.
Der tatsächliche Aufruf im Ordner \texttt{/mnt} erfolgt also in der Form \texttt{./setup\_raspberry.sh <MeinWlanName> <Passwort1234>}.

Sollte die Datei nicht ausführbar sein, muss der gesamte Inhalt des USB-Sticks auf das Dateisystem des Rechners kopiert werden.
Dort kann der Befehl \texttt{chmod +x setup\_rasp\-berry.sh} genutzt werden um die Datei ausführbar zu machen.

Das Skript prüft zu Beginn, ob eine Verbindung zum Internet besteht, indem versucht wird einen Ping zum DNS-Server von Google (IP-Adresse:~\texttt{8.8.8.8}) zu senden.
Falls keine Verbindung aufgebaut werden kann, sollte das Skript abgebrochen werden und die Fehlerursache untersucht werden.

Eine mögliche Ursache ist ein Fehlschlag bei der automatischen Konfiguration des WLANs.
In Tests ist es beispielsweise zu einem Fehler bei der SSID \glqq{}Fritz!Box 7362 SL\grqq{} gekommen, da das Ausrufezeichen fehlerhaft interpretiert wurde.

In solch einem Fall kann das WLAN manuell mit dem Tool \texttt{raspi-config} konfiguriert werden, wie es in Abschnitt~\ref{subsubsec:os-rpi-config-manual} beschrieben ist.
Danach kann das automatisierte Skript ohne die optionalen Parameter erneut aufgerufen werden.
Am Ende des Skripts wird das System neu gestartet.

Um die korrekte Ausführung des Skripts zu validieren, kann mit dem Befehl \texttt{docker ps} geprüft werden, ob beide gewünschten Container laufen.
Sollte dies nicht der Fall sein kann mit dem Skript \texttt{~/start\_containers.sh} erneut versucht werden die Container zu starten.

\subsubsection{Manuelle Konfiguration}\label{subsubsec:os-rpi-config-manual}
Ein großer Teil des Setups wird mit dem Tool \texttt{raspi-config} durchgeführt, das mit dem Befehl \texttt{sudo raspi-config} gestartet wird.
In diesem Tool werden die folgenden Einstellungen getätigt:

\begin{itemize}
    \item \textbf{Localisation \textrightarrow Keyboard}: Ein passendes Tastaturlayout konfigurieren.
    Üblicherweise ist hier die Auswahl \glqq{}Generic 105 keys\grqq{} mit deutschem Layout passend.
    Nach dieser Konfiguration kann das System neu gestartet werden, um das geänderte Layout zu aktivieren.
    \item \textbf{Interfaces}: Die Schnittstellen \glqq{}SSH\grqq{} und \glqq{}Legacy Camera\grqq{} aktivieren.
    \item \textbf{Performance \textrightarrow GPU Memory}: Es empfiehlt sich hier einem Wert von mindestens 256~MB einzustellen, um Probleme bei einer zu hohen Kameraauflösung zu vermeiden.
    \item \textbf{System \textrightarrow Wireless LAN}: Den WLAN-Zugang mit der korrekten SSID und dem richtigen Passwort einstellen.
\end{itemize}

Nach der Durchführung dieser Konfigurationen kann \texttt{raspi-config} geschlossen werden und das System neu gestartet werden.
Anschließend sollte mit den Befehlen \texttt{ip addr} und \texttt{ping 8.8.8.8} die Netzwerkverbindung geprüft werden.
Bei der Ausgabe von \texttt{ip addr} ist darauf zu achten, dass der Zustand der Netzwerkschnittstelle \texttt{wlan0} auf \texttt{UP} steht.
Wenn keine Netzwerkverbindung hergestellt werden konnte, muss zunächst die Ursache hierfür gefunden und behoben werden.

Nach dieser grundlegenden Konfiguration müssen noch die folgenden Schritte durchgeführt werden:
\begin{enumerate}
    \item {[optional]} Aktualisierung des Systems mit den Befehlen \texttt{sudo apt-get update} und \texttt{sudo apt-get full-upgrade}
    \item Installation von Docker mithilfe des offiziellen Hilfsskripts.
    Der Befehl hierzu lautet \texttt{curl -fsSL https://get.docker.com | /bin/sh}.
    Anschließend sollten dem Benutzer \texttt{pi} die Rechte zur direkten Bedienung von Docker zugeteilt werden, indem der Nutzer mit dem Befehl \texttt{sudo usermod -aG docker pi} zur entsprechenden Benutzergruppe hinzugefügt wird.
    \item Installation eines DHCP-Servers mit dem Befehl \texttt{sudo apt-get update \&\& sudo apt-get install isc-dhcp-server}
    \item Konfiguration des Netzwerks durch folgende Anpassungen:
    \begin{itemize}
        \item An die Datei \texttt{/etc/dhcpcd.conf} den folgenden Text anhängen:
        \begin{verbatim}
interface eth0
static ip\_address=192.168.217.1
        \end{verbatim}
        \item In der Datei \texttt{/etc/default/isc-dhcp-server} ersetzen: \texttt{INTERFACESv4=\textquotedblleft{}\textquotedblright{}} durch \texttt{INTERFACESv4=\textquotedblleft{}eth0\textquotedblright{}}
        \item An die Datei \texttt{/etc/dhcp/dhcpd.conf} folgendes anhängen:
        \begin{verbatim}
subnet 192.168.217.0 netmask 255.255.255.0 {
    range 192.168.217.5 192.168.217.50;
    option subnet-mask 255.255.255.0;
    option broadcast-address 192.168.217.255;
}
        \end{verbatim}
    \end{itemize}
    \item Erstellung der Hilfsskripte (diese werden im Abschnitt~\ref{sec:os-overview} erläutert):
    \begin{itemize}
        \item Den Inhalt von Listing~\ref{lst:os-rpi-config-manual-netscript-dhcp} in die Datei \texttt{\textasciitilde/network-scripts/dhcp\_ethernet.sh} ablegen
        \item Den Inhalt von Listing~\ref{lst:os-rpi-config-manual-netscript-direct} in die Datei \linebreak\texttt{\textasciitilde/network-scripts/direct\_access\_ethernet.sh} ablegen
        \item Den Inhalt von Listing~\ref{lst:os-rpi-config-manual-container} in die Datei \texttt{\textasciitilde/start\_containers.sh} ablegen
    \end{itemize}
    Diese Skripte müssen jeweils noch mit dem Befehl \texttt{chmod +x script.sh} ausführbar gemacht werden.
\end{enumerate}
\newpage

\begin{lstlisting}[language=bash, caption=Skript zum Zurücksetzen der Netzwerkeinstellungen auf DHCP für den Raspberry Pi, label=lst:os-rpi-config-manual-netscript-dhcp]
    #!/bin/bash
    #

    if [ "$EUID" -ne 0 ]; then
       echo "Dieses Skript muss als root ausgeführt werden"
       exit 1
    fi

    function replace_in_file() {
    sed {"s/$2/$3/g"} $1 > $1.tmp
    mv $1.tmp  $1
    }

    echo "> Deaktiviere DHCP-Server für eth0"
    replace_in_file /etc/default/isc-dhcp-server "INTERFACESv4=\"eth0\"" "INTERFACESv4=\"\""

    echo "> Deaktiviere statische IP für eth0"
    replace_in_file /etc/dhcpcd.conf "interface eth0"                  "#interface eth0"
    replace_in_file /etc/dhcpcd.conf "static ip_address=192.168.217.1" "#static ip_address=192.168.217.1"

    echo "> Konfiguration vollständig. Das System wird in 5 Sekunden neugestartet"
    sleep 5
    sudo reboot
\end{lstlisting}

\begin{lstlisting}[language=bash, caption=Skript zum Aktivieren des direkten Netzwerkmodus für den Raspberry Pi, label=lst:os-rpi-config-manual-netscript-direct]
    #!/bin/bash
    #

    if [ "$EUID" -ne 0 ]; then
       echo "Dieses Skript muss als root ausgeführt werden"
       exit 1
    fi

    function replace_in_file() {
    sed {"s/$2/$3/g"} $1 > $1.tmp
    mv $1.tmp  $1
    }

    echo "> Deaktiviere DHCP-Server für eth0"
    replace_in_file /etc/default/isc-dhcp-server "INTERFACESv4=\"\"" "INTERFACESv4=\"eth0\""

    echo "> Deaktiviere statische IP für eth0"
    replace_in_file /etc/dhcpcd.conf "#interface eth0"                  "interface eth0"
    replace_in_file /etc/dhcpcd.conf "#static ip_address=192.168.217.1" "static ip_address=192.168.217.1"

    echo "> Konfiguration vollständig. Das System wird in 5 Sekunden neugestartet"
    sleep 5
    sudo reboot
\end{lstlisting}
\newpage

\begin{lstlisting}[language=bash, caption=Skript zum Neustart der Container mit der Standardkonfiguration des Raspberry Pi, label=lst:os-rpi-config-manual-container]
    #!/bin/bash
    #

    # Set greater than 0 to disable video recording
    DISABLE_RECORDING=0

    # Settings. For Explanations lookup the documentation
    LOG_MAX_LEVEL=DEBUG

    SOURCE_TYPE=WEBCAM
    SOURCE_VIDEO_DEVICE_NO=0
    SOURCE_RES_WIDTH=640
    SOURCE_RES_HEIGHT=480

    YOLO_CONFIG_FILE=/etc/mintenv/yolo/yolo.cfg
    YOLO_WEIGHTS_FILE=/etc/mintenv/yolo/yolo.weights
    YOLO_NAMES_FILE=/etc/mintenv/yolo/yolo.names

    DETECTION_THRESHOLD=0.2
    DETECTION_MAX_OVERLAP=0.4
    DETECTION_BLOB_SIZE=416

    TARGET_RES_WIDTH=$SOURCE_RES_WIDTH
    TARGET_RES_HEIGHT=$SOURCE_RES_HEIGHT
    TARGET_FRAMERATE=20

    STREAM_HOST=127.0.0.1
    STREAM_PORT=8554
    STREAM_PATH=mintenv
    STREAM_USER=
    STREAM_PASS=

    function run_script() {
        echo "> Stoppe laufende Container"
        docker stop mintenv_streamserver
        docker stop mintenv_pipeline

        docker container rm mintenv_pipeline
        docker container rm mintenv_streamserver

        echo "> Starte Streamserver"
        docker run -d --net=host --restart=always \
                   --name=mintenv_streamserver \
                   -e DISABLE_RECORDING=$DISABLE_RECORDING \
                   -v /home/pi/mintenv/recordings:/home/rtsp/video \
                   menkalian/dhbw_studienarbeit_streamserver:latest

        echo "> Starte Pipeline"

        docker run -d --net=host --restart=always \
                   --name=mintenv_pipeline \
                   --device=/dev/video0 \
                   -e LOG_MAX_LEVEL=$LOG_MAX_LEVEL \
                   -e SOURCE_TYPE=$SOURCE_TYPE \
                   -e SOURCE_VIDEO_DEVICE_NO=$SOURCE_VIDEO_DEVICE_NO \
                   -e SOURCE_RES_WIDTH=$SOURCE_RES_WIDTH \
                   -e SOURCE_RES_HEIGHT=$SOURCE_RES_HEIGHT \
                   -e YOLO_CONFIG_FILE=$YOLO_CONFIG_FILE \
                   -e YOLO_WEIGHTS_FILE=$YOLO_WEIGHTS_FILE \
                   -e YOLO_NAMES_FILE=$YOLO_NAMES_FILE \
                   -e DETECTION_THRESHOLD=$DETECTION_THRESHOLD \
                   -e DETECTION_MAX_OVERLAP=$DETECTION_MAX_OVERLAP \
                   -e DETECTION_BLOB_SIZE=$DETECTION_BLOB_SIZE \
                   -e TARGET_RES_WIDTH=$TARGET_RES_WIDTH \
                   -e TARGET_RES_HEIGHT=$TARGET_RES_HEIGHT \
                   -e TARGET_FRAMERATE=$TARGET_FRAMERATE \
                   -e STREAM_HOST=$STREAM_HOST \
                   -e STREAM_PORT=$STREAM_PORT \
                   -e STREAM_PATH=$STREAM_PATH \
                   -e STREAM_USER=$STREAM_USER \
                   -e STREAM_PASS=$STREAM_PASS \
                   menkalian/dhbw_studienarbeit_pipeline:raspi
    }

    run_script
\end{lstlisting}

\newpage


\section{Jetson Nano}\label{sec:os-jet}
Dieser Abschnitt beschreibt detailliert die Schritte, die zum Setup einer frischen Instanz des Betriebssystems durchgeführt werden müssen.
Dabei wird der Hardwareaufbau wie er im Abschnitt~\ref{subsec:hardware-jet-setup} beschrieben ist vorausgesetzt.

\subsection{Flashen des Images auf eine MicroSD-Karte}\label{subsec:os-jet-flash}

Um das Betriebssystem des Jetson Nano auf eine MicroSD-Karte zu schreiben kann ebenfalls der Raspberry Pi Imager verwendet werden, wie in Abschnitt~\ref{subsec:os-rpi-flash} beschrieben ist.
Die Funktionalität wurde mit der Version 4.6 getestet.
Diese kann unter der URL \texttt{https://developer.nvidia.com/embedded/l4t/r32\_release\_v6.1/jeston\_nano/\newline{}jetson-nano-jp46-sd-card-image.zip} heruntergeladen werden (der Schreibfehler \newline\texttt{jeston\_nano} ist so korrekt und wurde durch NVIDIA verursacht).

Um das Image zu verwenden, muss die heruntergeladene ZIP-Datei entpackt werden.
Die enthaltene \texttt{.img}-Datei ist im Imager als Betriebssystem auszuwählen.

\subsection{Konfiguration des Betriebssystems}\label{subsec:os-jet-config}
Beim ersten Start des Systems beginnt ein geführter Setup-Prozess.

Nachdem die Lizenz akzeptiert wurde, können Sprache, Tastaturlayout und Zeitzone gewählt werden.
Danach folgt die Konfiguration des Standardbenutzers, wie sie in Abbildung~\ref{fig:os-jet-config-user} gezeigt ist.

\begin{figure} [htpb]
    \centering
    \includegraphics[width=0.7\columnwidth]{img/os/jet/02_user}
    \caption{Eingabe des Standardbenutzers}
    \label{fig:os-jet-config-user}
\end{figure}

Die Werte für den Benutzer können beliebig gewählt werden.
In den gezeigten Screenshots wurde jeweils der Benutzer \texttt{dhbw} genutzt.

Nach der Eingabe des Benutzers erfolgt die Konfiguration der Partitionsgröße.
Hier sollte, entsprechend des gesetzten Standards, die maximale Größe gewählt werden.

Darauf folgt die Auswahl des Performancemodus.
Hier sollte die Einstellung \glqq{}MAXN\grqq{} gewählt werden, da ansonsten nur ein Teil der Rechenleistung verfügbar ist..

Nachdem alle Einstellungen getätigt wurden, wird die Konfiguration angewendet und das System wird neugestartet.
Nach der Anmeldung erscheint dann der Desktop und der zweite Schritt des Setups kann durchgeführt werden.

Dafür kann ein automatisiertes Skript verwendet werden oder die nötigen Teilschritte können manuell durchgeführt werden.
Die nächsten Abschnitte beschreiben das Vorgehen für beide Alternativen.

\subsubsection{Nutzung des automatisierten Skripts}\label{subsubsec:os-jet-config-script}
Die prinzipielle Anwendung des Skripts funktioniert wie es für den Raspberry Pi in Abschnitt~\ref{subsubsec:os-rpi-config-script} beschrieben ist.
Für den Jetson Nano muss jedoch die Datei \texttt{setup\_jetson.sh} \textbf{ohne Parameter} ausgeführt werden.

\subsubsection{Manuelle Konfiguration}\label{subsubsec:os-jet-config-manual}

Sollte das Setup-Skript nicht verwendet werden, so müssen die folgenden Schritte durchgeführt werden:
\begin{enumerate}
    \item {[optional]} Aktualisierung des Systems mit den Befehlen \texttt{sudo apt-get update} und \texttt{sudo apt-get full-upgrade}
    \item Konfiguration von Docker zur direkten Bedienung durch den eigenen Benutzer.
    Hierzu wird der Befehl \texttt{sudo usermod -aG docker \$(whoami)} verwendet.
    Nach diesem Schritt wird ein Neustart empfohlen, damit die geänderten Rechte geladen werden.
    \item Konfiguration des Netzwerks durch folgende Anpassungen:
    \begin{itemize}
        \item In die Datei \texttt{/etc/network/interfaces} den folgenden Text einfügen (hierbei ist auf die Einrückung und die Position des \glqq{}\#\grqq{}-Zeichen achten):
        \begin{verbatim}
#auto eth0
#iface eth0 inet static
  #address 192.168.217.1
  #netmask 255.255.255.0
        \end{verbatim}
        \item An die Datei \texttt{/etc/dhcp/dhcpd.conf} folgendes anhängen:
        \begin{verbatim}
subnet 192.168.217.0 netmask 255.255.255.0 {
    range 192.168.217.5 192.168.217.50;
    option subnet-mask 255.255.255.0;
    option broadcast-address 192.168.217.255;
}
        \end{verbatim}
    \end{itemize}
    \item Erstellung der Hilfsskripte (diese werden im Abschnitt~\ref{sec:os-overview} erläutert):
    \begin{itemize}
        \item Den Inhalt von Listing~\ref{lst:os-jet-config-manual-netscript-dhcp} in die Datei \texttt{\textasciitilde/network-scripts/dhcp\_ethernet.sh} ablegen
        \item Den Inhalt von Listing~\ref{lst:os-jet-config-manual-netscript-direct} in die Datei \linebreak\texttt{\textasciitilde/network-scripts/direct\_access\_ethernet.sh} ablegen
        \item Den Inhalt von Listing~\ref{lst:os-jet-config-manual-container} in die Datei \texttt{\textasciitilde/start\_containers.sh} ablegen
    \end{itemize}
    Diese Skripte müssen jeweils noch mit dem Befehl \texttt{chmod +x script.sh} ausführbar gemacht werden.

\end{enumerate}
\newpage

\begin{lstlisting}[language=bash, caption=Skript zum Zurücksetzen der Netzwerkeinstellungen auf DHCP für den Jetson Nano, label=lst:os-jet-config-manual-netscript-dhcp]
    #!/bin/bash
    #

    if [ "$EUID" -ne 0 ]; then
       echo "Dieses Skript muss als root ausgeführt werden"
       exit 1
    fi

    function replace_in_file() {
        sed {"s/$2/$3/g"} $1 > $1.tmp
        mv $1.tmp  $1
    }

    echo "> Deaktiviere DHCP-Server für eth0"
    replace_in_file /etc/default/isc-dhcp-server "INTERFACESv4=\"eth0\"" "INTERFACESv4=\"\""

    echo "> Deaktiviere statische IP für eth0"
    replace_in_file /etc/network/interfaces "auto eth0"              "#auto eth0"
    replace_in_file /etc/network/interfaces "iface eth0 inet static" "#iface eth0 inet static"
    replace_in_file /etc/network/interfaces "address 192.168.217.1"  "#address 192.168.217.1"
    replace_in_file /etc/network/interfaces "netmask 255.255.255.0"  "#netmask 255.255.255.0"

    systemctl disable isc-dhcp-server
    echo "> Konfiguration vollständig. Das System wird in 5 Sekunden neugestartet"
    sleep 5
    reboot
\end{lstlisting}

\begin{lstlisting}[language=bash, caption=Skript zum Aktivieren des direkten Netzwerkmodus für den Jetson Nano, label=lst:os-jet-config-manual-netscript-direct]
    #!/bin/bash
    #

    if [ "\$EUID" -ne 0 ]; then
       echo "Dieses Skript muss als root ausgeführt werden"
       exit 1
    fi

    function replace_in_file() {
    sed {"s/$2/$3/g"} $1 > $1.tmp
    mv $1.tmp  $1
    }

    echo "> Deaktiviere DHCP-Server für eth0"
    replace_in_file /etc/default/isc-dhcp-server "INTERFACESv4=\"eth0\"" "INTERFACESv4=\"\""

    echo "> Deaktiviere statische IP für eth0"
    replace_in_file /etc/network/interfaces "#auto eth0"              "auto eth0"
    replace_in_file /etc/network/interfaces "#iface eth0 inet static" "iface eth0 inet static"
    replace_in_file /etc/network/interfaces "#address 192.168.217.1"  "address 192.168.217.1"
    replace_in_file /etc/network/interfaces "#netmask 255.255.255.0"  "netmask 255.255.255.0"

    systemctl enable isc-dhcp-server

    echo "> Konfiguration vollständig. Das System wird in 5 Sekunden neugestartet"
    sleep 5
    reboot
\end{lstlisting}
\newpage
\begin{lstlisting}[language=bash, caption=Skript zum Neustart der Container mit der Standardkonfiguration des Jetson Nano, label=lst:os-jet-config-manual-container]
    #!/bin/bash
    #

    # Set greater than 0 to disable video recording
    DISABLE_RECORDING=0

    # Settings. For Explanations lookup the documentation
    LOG_MAX_LEVEL=DEBUG

    SOURCE_TYPE=ZED2
    SOURCE_VIDEO_DEVICE_NO=0
    SOURCE_RES_WIDTH=1280
    SOURCE_RES_HEIGHT=720

    YOLO_CONFIG_FILE=/etc/mintenv/yolo/yolo.cfg
    YOLO_WEIGHTS_FILE=/etc/mintenv/yolo/yolo.weights
    YOLO_NAMES_FILE=/etc/mintenv/yolo/yolo.names

    DETECTION_THRESHOLD=0.2
    DETECTION_MAX_OVERLAP=0.4
    DETECTION_BLOB_SIZE=832

    TARGET_RES_WIDTH=$SOURCE_RES_WIDTH
    TARGET_RES_HEIGHT=$SOURCE_RES_HEIGHT
    TARGET_FRAMERATE=60

    STREAM_HOST=127.0.0.1
    STREAM_PORT=8554
    STREAM_PATH=mintenv
    STREAM_USER=
    STREAM_PASS=

    function run_script() {
        echo "> Stoppe laufende Container"
        docker stop mintenv_streamserver                                                               > /dev/null 2>/dev/null
        docker stop mintenv_pipeline                                                                   > /dev/null 2>/dev/null

        docker container rm mintenv_pipeline                                                           > /dev/null 2>/dev/null
        docker container rm mintenv_streamserver                                                       > /dev/null 2>/dev/null

        echo "> Starte Streamserver"
        docker run -d --net=host --restart=always \
                   --name=mintenv_streamserver \
                   -e DISABLE_RECORDING=\$DISABLE_RECORDING \
                   -v ~/mintenv/recordings:/home/rtsp/video \
                   menkalian/dhbw_studienarbeit_streamserver:latest

        echo "> Starte Pipeline"
        docker run -d --net=host --restart=always \
                   --name=mintenv_pipeline \
                   --privileged \
                   -v ~/mintenv/zed2_calibrations:/usr/local/zed/settings \
                   -e LOG_MAX_LEVEL=$LOG_MAX_LEVEL \
                   -e SOURCE_TYPE=$SOURCE_TYPE \
                   -e SOURCE_VIDEO_DEVICE_NO=$SOURCE_VIDEO_DEVICE_NO \
                   -e SOURCE_RES_WIDTH=$SOURCE_RES_WIDTH \
                   -e SOURCE_RES_HEIGHT=$SOURCE_RES_HEIGHT \
                   -e YOLO_CONFIG_FILE=$YOLO_CONFIG_FILE \
                   -e YOLO_WEIGHTS_FILE=$YOLO_WEIGHTS_FILE \
                   -e YOLO_NAMES_FILE=$YOLO_NAMES_FILE \
                   -e DETECTION_THRESHOLD=$DETECTION_THRESHOLD \
                   -e DETECTION_MAX_OVERLAP=$DETECTION_MAX_OVERLAP \
                   -e DETECTION_BLOB_SIZE=$DETECTION_BLOB_SIZE \
                   -e TARGET_RES_WIDTH=$TARGET_RES_WIDTH \
                   -e TARGET_RES_HEIGHT=$TARGET_RES_HEIGHT \
                   -e TARGET_FRAMERATE=$TARGET_FRAMERATE \
                   -e STREAM_HOST=$STREAM_HOST \
                   -e STREAM_PORT=$STREAM_PORT \
                   -e STREAM_PATH=$STREAM_PATH \
                   -e STREAM_USER=$STREAM_USER \
                   -e STREAM_PASS=$STREAM_PASS \
                   menkalian/dhbw_studienarbeit_pipeline:jetson
    }

    run_script
\end{lstlisting}

\newpage


\section{Funktionen im konfigurierten Betriebssystem}\label{sec:os-overview}
Durch die Durchführung des Setups wird auf beiden Systemen eine Umgebung geschaffen, die einige grundlegende Funktionen zur Verfügung stellt.
In diesem Abschnitt werden die wichtigsten dieser Funktionen erwähnt und einige Anwendungshinweise gegeben.

Der Zugriff auf die Systeme ist jeweils über SSH mit der Angabe eines Passworts möglich.
Die IP-Adresse für den Zugriff ist jeweils abhängig vom Netzwerkmodus zu bestimmen.

Auf den Systemen ist zudem jeweils Docker als Containerumgebung verfügbar.
Die Docker-Container haben in der Standardkonfiguration geteilte Verzeichnisse mit dem Host-Betriebssystem.
Diese sind Unterverzeichnisse des Ordners \texttt{\textasciitilde/mintenv}.
Zum einen werden hier im Unterordner \texttt{zed2\_calibrations} die Kalibrierungsdaten der ZED2-Kameras abgelegt.
Im Unterverzeichnis \texttt{recordings} werden die Aufzeichnungen des Videostreams gespeichert.

Hier ist darauf zu achten, dass die Videoaufnahmen keine Begrenzung bezüglich des Speicherverbrauchs haben und beliebig groß werden können.
Um zu verhindern, dass der Speicherplatz der SD-Karte aufgebraucht wird, gibt es zwei empfohlene Vorgehensweisen:
\begin{itemize}
    \item Die Videoaufzeichnung kann in der Konfiguration vollständig abgeschalten werden.
    Nach einer Änderung der Konfiguration sind die Container neu zu starten.
    \item Ein weiterer Datenträger kann an der Stelle des Ordners in das Dateisystem eingehängt werden.
    Bei diesem Verfahren wird nun der Speicherplatz des externen Datenträgers aufgebraucht anstelle der SD-Karte.
\end{itemize}

Auf dem System sind zudem einige Hilfsskripte vorhanden.
Das Skript \newline\texttt{\textasciitilde/start\_containers.sh} wird zum Starten und Konfigurieren der Container verwendet.
Die Konfiguration wird durchgeführt indem die Kontanten in der Datei bearbeitet werden.

Zwei weitere Hilfsskripte sind \texttt{\textasciitilde/network-scripts/dhcp\_ethernet.sh} und \texttt{~/network-scripts/direct\_access\_ethernet.sh}.
Diese Skripte werden verwendet, um zwischen den vorbereiteten Netzwerkkonfigurationen zu wechseln.
Die folgenden Netzwerkkonfigurationen werden auf den Systemen unterstützt:
\begin{itemize}
    \item \glqq{}DHCP-Modus\grqq{}: In diesem Modus wird die Netzwerkkonfiguration des Systems automatisch über DHCP bezogen, sobald es an ein bestehendes Netzwerk angeschlossen wird.
    In diesem Modus ist Zugriff auf das Internet möglich, falls der entsprechende Zugang im Netzwerk bereitgestellt ist (beispielsweise durch einen Router).
    Die IP-Adresse kann entweder beim DHCP-Server des Netzwerks (in der Regel ist dies der Router) oder durch den Befehl \texttt{ip addr} ermittelt werden.
    \item \glqq{}Direct-Access-Modus\grqq{}: In diesem Modus hat das System eine statische IP-Adresse: \texttt{192.168.217.1}.
    Zudem läuft dann auf dem System ein DHCP-Server, wodurch ein anderes Gerät direkt mit einem Netzwerkkabel für direkten Zugriff verbunden werden kann.
    Hierbei ist zu beachten, dass die Kabelverbindung \textbf{vor} dem Start des Systems hergestellt werden muss.
    In diesem Modus ist über eine kabelgebundene Verbindung (\texttt{eth0}) \textbf{kein} Zugriff auf das Internet möglich.
    Eine kabellose Verbindung zum Internet über WLAN ist weiterhin möglich.
\end{itemize}

Nach dem automatischen Setup befindet sich der Raspberry Pi im \glqq{}Direct-Access-Modus\grqq{}, während sich der Jetson Nano im \glqq{}DHCP-Modus\grqq{} befindet.
