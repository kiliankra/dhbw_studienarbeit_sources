\chapter{Training der Bilderkennung}\label{ch:training}

Zum Training der Bilderkennung sind im Git-Repository des Projekts (\url{https://gitlab.com/kiliankra/dhbw_studienarbeit_sources}) unter dem Pfad \texttt{training/scripts} einige Hilfsskripte und Dateien bereitgestellt.
Das Datenset des Trainings wird über die Datei \texttt{training\_config.json} gesteuert, deren Aufbau in Abschnitt~\ref{sec:training-trainingconfig} beschrieben wird.

Zum Start des Trainings wird die Datei \texttt{start\_training.sh} ausgeführt.
Zur Durchführung des Trainings wird CUDA benötigt.
Dies ist auf dem Jetson Nano bereits installiert, jedoch befindet sich der CUDA-Compiler \texttt{nvcc} nicht im Standardpfad.
Um dies zu korrigieren wird der Befehl \texttt{export PATH=\$PATH:/usr/local/cuda/bin} verwendet.
Falls das Training auf einem anderen System durchgeführt wird, muss dort CUDA entsprechend der offiziellen Anleitung von NVIDIA installiert werden.

Das Trainingsskript installiert zunächst einige benötigte Komponenten für das Training, wie beispielsweise \texttt{python3}, \texttt{cmake} und \texttt{git}.
Anschließend wird \texttt{darknet} von Github geklont und kompiliert, falls es nicht bereits im Standardpfad des Systems vorhanden ist.
Zudem werden Startgewichte für das YOLO-Modell heruntergeladen.

Möglicherweise ist die CMake-Version, die durch \texttt{apt} installiert wird, nicht kompatibel mit dem \texttt{darknet}-Repository.
In diesem Fall sollte eine passende Version von \url{https://cmake.org/download/} heruntergeladen und mit einem Befehl der Form \texttt{sudo ./cmake-3.23.1-linux-aarch64.sh --prefix=/usr} installiert werden.

Danach wird das Python-Skript gestartet, das die Trainingsdaten vorbereitet.
Dieses erwartet zunächst eine Angabe, ob das COCO-Dataset, das OpenImages-Dataset oder beide verwendet werden sollen.
Danach werden die benötigten Daten heruntergeladen und in das benötigte Format für YOLO exportiert.

Für die Verarbeitung des OpenImages-Dataset ist es nötig den Arbeitsspeicher des Jetson-Nano durch Swap zu erweitern.
Eine Anleitung hierzu findet sich unter~\url{https://jetsonhacks.com/2019/11/28/jetson-nano-even-more-swap/}.
Welcher genaue Wert benötigt wird, ist abhängig von den benötigten Objektklassen.
Es sollten jedoch mindestens 32~GB Arbeitsspeicher vorhanden sein.

Für das Training mit dem COCO-Datenset werden circa 100~GB freier Festplattenspeicher benötigt.
Diese können bei Bedarf auch durch einen zusätzlichen (externen) Datenträger bereitgestellt werden.

Für ein Training mit beiden Datensets (COCO und OpenImages) sollte ein separates System mit mehr Arbeits- und Festplattenspeicher verwendet werden.

Hiernach wird das Training mit \texttt{darknet} gestartet.
Dieses kann, je nach der Größe des Datensets, sehr lange dauern.
Daher ist es empfehlenswert den Trainingsprozess mithilfe des Tools \texttt{screen} im Hintergrund laufen zu lassen.


\section{Anlegen eines Templates für die YOLO-Konfiguration}\label{sec:training-configtemplate}
Die Grundlage für das Template bietet eine Konfiguration aus dem Git-Repository der Bilderkennung (\url{https://github.com/AlexeyAB/darknet}).
Dort ist in der Datei \texttt{README.md} auch beschrieben wie diese Konfiguration anhand der Trainingsdaten modifiziert werden muss.
Um diese Anpassung der Konfiguration automatisieren zu können wird die tatsächliche YOLO-Konfiguration aus einem Template erzeugt.
Dabei werden die Platzhalter aus Tabelle~\ref{tab:training-configtemplate-placeholders} entsprechend der Anweisungen aus dem Repository eingesetzt.

\begin{table}
    \centering
    \caption{Definierte Platzhalter für das Konfigurationstemplate}
    \label{tab:training-configtemplate-placeholders}
    \begin{tabular}{|c|c|}
        \textbf{Platzhalter}                       & \textbf{Eingesetzter Wert}            \\
        \texttt{\{\{Classes.Amount\}\}}            & Anzahl der Klassen im Datenset        \\
        \texttt{\{\{Classes.Filter.Amount\}\}}     & Berechnete Filteranzahl               \\
        \texttt{\{\{Batches.Max\}\}}               & Maximale Anzahl von Batches           \\
        \texttt{\{\{Batches.Percentile.Eighty\}\}} & 80\% der maximalen Anzahl von Batches \\
        \texttt{\{\{Batches.Percentile.Ninety\}\}} & 90\% der maximalen Anzahl von Batches \\
    \end{tabular}
\end{table}


\section{Aufbau einer Trainingskonfiguration}\label{sec:training-trainingconfig}

Die Trainingskonfiguration ist eine einfache Datei im JSON-Format.
Eine Beispielkonfiguration ist in Listing~\ref{lst:training-trainingconfig-example} gezeigt.
Das enthaltene JSON-Objekt hat ein Element namens \glqq{}\texttt{classes}\grqq{}.

Dieses Element enthält wiederum ein Objekt, das ein Element pro Klasse im Datenset enthält.
Jeder Klasse können mehrere Klassen aus dem COCO-Datenset oder dem OpenImagesV6-Datenset zugeordnet werden.
Die Liste der Klassen kann auf der Webseite des jeweiligen Datensets betrachtet werden.
Der angezeigte Name ist der Name des Elements innerhalb des \texttt{classes}-Objektes.

\begin{lstlisting}[caption=Beispielhafte Trainingskonfiguration für 3 Klassen, label=lst:training-trainingconfig-example]
{
  "classes":{
    "Banane": {
      "coco": ["banana"],
      "oid" : ["Banana"]
    },
    "Uhr": {
      "coco": ["clock"],
      "oid" : ["Clock"]
    },
    "Handy": {
      "coco": ["cell phone"],
      "oid" : ["Mobile phone"]
    }
  }
}
\end{lstlisting}
