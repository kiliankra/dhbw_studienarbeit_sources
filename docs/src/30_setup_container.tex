\chapter{Aufbau der Docker-Container}\label{ch:docker}
Die Softwarekomponenten der Implementierung sind in Docker-Containern isoliert.
Dabei gibt es zwei Komponenten: Den Streamingserver, der für die Verwaltung und Aufzeichnung des Videostreams verantwortlich ist, und die Pipeline, die Bilder von der Kamera liest und zum Quellvideo verarbeitet.

Die Dateien zum Bauen der Docker-Images liegen im Repository unter dem Pfad \texttt{docker/\textit{<Komponente>}} ab.
Die jeweiligen \texttt{Dockerfile}-Dateien enthalten den Befehl der benötigt wird um das Image zu bauen.


\section{Streamingserver}\label{sec:docker-streamserver}

Für den Streamingserver wird die quelloffene Implementierung \texttt{rtsp-simple-server} in der Version \texttt{0.17.17} verwendet.
Der Quellcode kann unter \url{https://github.com/aler9/rtsp-simple-server/tree/v0.17.17} gefunden werden.

\subsection{Aufbau des Images}\label{subsec:docker-streamserver-image}
Das Docker-Image des Streamingservers wird aufgebaut indem die ausführbare Datei \texttt{rtsp-simple-server} aus dem offiziellen Docker-Image kopiert wird und das Tool \texttt{ffmpeg} installiert wird.
Dieses wird genutzt, um den Videostream aufzuzeichnen.
Die Konfiguration des Servers und ein Skript für den Start der Videoaufzeichnung werden aus dem Ordner in das Image kopiert.

\subsection{Konfigurationsmöglichkeiten}\label{subsec:docker-streamserver-config}

Die Konfiguration des Streamingservers erfolgt größtenteils über die Konfiguration, die im Docker-Image abgelegt ist.
Die Parameter dieser Konfigurationsdatei wird im Repository der Komponente erläutert.
Bei der verwendeten Konfiguration ist das RTSP-Protokoll aktiv, es findet keine Authentifizierung statt und sobald ein Videostream startet, wird dieser aufgezeichnet.

Abseits dieser Konfiguration kann das Verhalten des Servers über die Umgebungsvariable \texttt{DISABLE\_RECORDING} gesteuert werden.
Ist diese auf einen Wert $>0$ gesetzt, so wird der Videostream nicht aufgezeichet.
Falls die Variable nicht gesetzt ist oder einen Wert $\leq 0$ hat, findet eine Aufzeichnung statt.


\section{Pipeline}\label{sec:docker-pipeline}

Die Pipeline-Anwendung ist im Rahmen dieser Studienarbeit entwickelt worden und implementiert die Ansteuerung von OpenCV, GStreamer und der ZED-Kamera-Bibliothek.

\subsection{Aufbau des Images}\label{subsec:docker-pipeline-image}
Das Image für die Pipeline wird in zwei Schritten aufgebaut.
Im ersten Schritt wird eine Buildumgebung aufgebaut, indem die benötigten Programme und Bibliotheken installiert werden.
Diese Installation erfolgt entweder aus dem \texttt{apt}-Repository oder indem die Software aus dem Quellcode kompiliert wird.

Die benötigte Umgebung ist abhängig davon ob der Raspberry Pi oder der Jetson Nano verwendet werden.
Für beide Varianten werden OpenCV, GStreamer und C++-Entwicklungstools installiert.
OpenCV wird für den Raspberry Pi mit CPU-Optimierung und für den Jetson Nano mit GPU-Optimierung kompiliert.

Beim Jetson Nano werden zudem die Stereolabs SDK für die ZED2-Kamera und diverse CUDA-Bibliotheken installiert.

Diese Softwareumgebung ist in einem Docker-Image auf Docker-Hub unter dem Namen \texttt{menkalian/dhbw\_studienarbeit\_pipeline\_env} veröffentlicht.
Das Bauen dieser Umgebung benötigt mehrere Stunden.

Auf Basis dieses Images wird nun das tatsächliche Pipeline-Image erzeugt, indem der Quellcode der Pipeline im Image kompiliert wird.
Zudem werden die aktuellen Trainingsergebnisse aus dem Ordner \texttt{training/results/latest} im Image abgelegt.

\subsection{Architektur der Software}\label{subsec:docker-pipeline-software}

Die Softwarearchitektur basiert auf dem \texttt{Pipeline}-Pattern, bei dem Daten über mehrere Schritte verarbeitet werden.
Die Klasse \texttt{ProcessingPipeline} implementiert dieses Pattern und besteht aus einer \texttt{ImageSource} und mehreren \texttt{ImageTransformer}-Schritten.
Um die Architektur möglichst simpel zu halten, wird die Ausgabe des Videostreams ebenfalls als ein Verarbeitungsschritt (\texttt{ImageTransformer}) in der Pipeline umgesetzt.

Die Pipeline wird in der Methode \texttt{main} instanziiert und mit den benötigten Verarbeitungsschritten initialisiert.
Die Videoquelle wird abhängig vom Parameter \texttt{SOURCE\_TYPE} gewählt.
Die grundsätzliche Reihenfolge der Schritte ist \texttt{YoloDetector} \textrightarrow \texttt{LabelWriter} \textrightarrow \texttt{StreamOutput}.

Bei der \texttt{Zed2CameraSource} ist die Objektdetektion bereits enthalten, daher wird der Schritt \texttt{YoloDetector} in der Pipeline deaktiviert, falls dieser Quelltyp verwendet wird.


Die Parameter des Programms werden in der Klasse \texttt{Configuration} aus den Umgebungsvariablen bestimmt.
\texttt{DetectionUtil} ist eine Hilfsklasse für die Bilderkennung, die von \texttt{Zed2CameraSource} und \texttt{YoloDetector} verwendet wird.

Die Klasse \texttt{Image} ist eine Datenstruktur, die zum Austausch der Daten zwischen den einzelnen Verarbeitungschritten dient.
Die Klasse enthält die Grafikdaten eines Bildes, sowie Metadaten zu verschiedenen Regionen des Bildes (beispielsweise detektierte Objektklasse oder die Sicherheit bezüglich dieser Klassifizierung).

Die Beendigung des Programms wird durch UNIX-Signale gesteuert, da diese Signale von Docker verwendet werden, um einen Container zu beenden.
Sobald eines der Signale \texttt{SIGTERM} oder \texttt{SIGINT} an die Applikation gesendet wird, started die ordentliche Deinitialisierung des Programms und anschließend wird dieses beendet.

\subsection{Konfigurationsmöglichkeiten}\label{subsec:docker-pipeline-config}
Die Konfiguration der Pipeline steuert einen großen Teil des Systemverhaltens.
Um die Konfiguration über einen Docker-Container möglichst einfach zu gestalten, werden Umgebungsvariablen zur Steuerung verwendet.
Diese können beim Start des Containers mit dem Parameter \texttt{-e VARIABLE=VALUE} gesetzt werden.
Tabelle~\ref{tab:docker-pipeline-config-list} listet alle verfügbaren Parameter mit Erklärung auf.

Die Parameter der Bilderkennung werden in drei Dateien angegeben.
Diese Dateien können über ein Docker-Volume in einem Container ersetzt werden.

%! suppress = LineBreak
\begin{longtable}{c | p{0.25\columnwidth} | p{0.4\columnwidth}}
    \textbf{Variablenname}             & \textbf{Beispielwert}                            & \textbf{Erklärung}                                                                                                                                   \\
    \hline
    \texttt{LOG\_MAX\_LEVEL}           & \textit{DEBUG}                                   & Zu verwendendes Loglevel. Zulässige Werte sind \texttt{ERROR}, \texttt{WARN}, \texttt{INFO}, \texttt{DEBUG} und \texttt{TRACE}                       \\
    \texttt{SOURCE\_TYPE}              & \textit{ZED2}                                    & Videoquelle, die verwendet werden soll. Zulässige Werte sind \glqq{}WEBCAM\grqq{} und \glqq{}ZED2\grqq{} (für den Jetson Nano).                      \\
    \texttt{SOURCE\_VIDEO\_DEVICE\_NO} & \textit{0}                                       & ID des Video-Device das verwendet werden soll (z.B. \texttt{/dev/video0}). Wird nur beim Quelltyp \glqq{}WEBCAM\grqq{} verwendet.                    \\
    \texttt{SOURCE\_RES\_WIDTH}        & \textit{1280}                                    & Breite der Videoauflösung die von der Kamera gelesen wird                                                                                            \\
    \texttt{SOURCE\_RES\_HEIGHT}       & \textit{720}                                     & Höhe der Videoauflösung die von der Kamera gelesen wird                                                                                              \\
    \texttt{YOLO\_CONFIG\_FILE}        & \textit{/etc/mintenv/yolo/yolo.cfg}              & Datei aus der die YOLO/darknet Konfiguration gelesen wird. Die Konfiguration wird beim Training des Modells erzeugt.                                 \\
    \texttt{YOLO\_WEIGHTS\_FILE}       & \textit{/etc/mintenv/yolo/yolo.weights}          & Datei aus der die trainierten YOLO/darknet Gewichte für die Bilderkennung gelesen werden                                                             \\
    \texttt{YOLO\_NAMES\_FILE}         & \textit{/etc/mintenv/yolo/yolo.names}            & Datei aus der die Namen der erkennbaren Objektklassen gelesen werden                                                                                 \\
    \texttt{DETECTION\_THRESHOLD}      & \textit{0.2}                                     & Schwellwert für die benötigte Sicherheit einer Objektvorhersage, damit diese angezeigt wird                                                          \\
    \texttt{DETECTION\_MAX\_OVERLAP}   & \textit{0.4}                                     & Maximale Überschneidung einer Objektdetektion zu einem anderen erkannten Objekt.                                                                     \\
    \texttt{DETECTION\_BLOB\_SIZE}     & \textit{832}                                     & Dimension des Datenblobs, der an OpenCV für die Bilderkennung übergeben wird. Ein fehlerhafter Wert dieses Parameters führt zu einem Absturz.        \\
    \texttt{TARGET\_RES\_WIDTH}        & \textit{1280}                                    & Breite der Videoauflösung des ausgegebenen Streams                                                                                                   \\
    \texttt{TARGET\_RES\_HEIGHT}       & \textit{720}                                     & Höhe der Videoauflösung des ausgegebenen Streams                                                                                                     \\
    \texttt{TARGET\_FRAMERATE}         & \textit{40}                                      & Anzahl der Bilder, die pro Sekunde in den Videostream geschrieben werden. Dies ist gleichzeitig die maximale Bildrate des Videostreams.              \\
    \texttt{STREAM\_HOST}              & \textit{127.0.0.1}                               & IP-Adresse oder Hostname des Streamservers.                                                                                                          \\
    \texttt{STREAM\_PORT}              & \textit{8554}                                    & RTSP-Port des Streamservers                                                                                                                          \\
    \texttt{STREAM\_PATH}              & \textit{mintenv}                                 & Eindeutiger Pfad/Kennung des Streams beim Streamingserver. Über diesen Namen wird der Stream zum Zuschauen abgerufen.                                \\
    \texttt{STREAM\_USER}              & \textit{dhbw}                                    & Benutzername zur Authentifizierung am Streamserver. In der Standardkonfiguration muss dieser Wert leer sein, da keine Authentifizierung stattfindet. \\
    \texttt{STREAM\_PASS}              & \textit{passwort}                                & Passwort zur Authentifizierung am Streamserver. In der Standardkonfiguration muss dieser Wert leer sein, da keine Authentifizierung stattfindet.     \\
    \texttt{STREAM\_URL}               & \textit{rtsp://my-streaming-server.de:8554/dhbw} & Vollständige URL für den Streamserver. Falls dieser Wert nicht gesetzt ist, wird die URL aus den obigen Parametern berechnet.                        \\

    \caption{Auflistung der Konfigurationsparameter}
    \label{tab:docker-pipeline-config-list}
\end{longtable}

Bei der eingehenden Videoauflösung ist darauf zu achten, dass diese Auflösung von der Kamera unterstützt wird.
Für die ZED2-Kamera werden lediglich 2 Auflösungen akzeptiert (\texttt{1280x720} und \texttt{1920x1080}).
Für allgemeine Videogeräte, wie ein Raspberry Pi Kameramodul, kann mit dem Befehl \texttt{v4l2-ctl --list-formats-ext} angezeigt werden.
Für die Raspberry Pi Kamera HQ werden beispielsweise nur Auflösungen im Verhältnis \texttt{4:3} akzeptiert.
Eine fehlerhafte Videoauflösung kann zu falschen Bilddaten oder dem Absturz des Programms führen.

\subsection{Performanceoptimierungen}\label{subsec:docker-pipeline-optimization}

Für die Implementierung wurden die folgenden Performanceoptimierungen vorgenommen:
\begin{itemize}
    \item Bevorzugung der OpenCV-Implementierung der YOLO-Bilderkennung gegenüber der ursprünglichen \texttt{darknet}-Implementierung, aufgrund besserer Performance.
    \item Verwendung der GStreamer-Bibliothek (mit OpenCV), aufgrund einer geringeren Latenz im Vergleich zur \texttt{ffmpeg}/\texttt{libav} Bibliothek.
    \item Konfiguration von GStreamer auf minimale Latenz beim Aufruf (\glqq{}latency=0\grqq{})
    \item Nutzung des moderneren RTSP-Protokolls anstelle des RTMP-Protokolls
    \item Encodierung des Videos im MJPEG-Format, da dieses weniger Rechenleistung benötigt als H264- und H265-Encoding (trotz Hardwareunterstützung).
    \item Verwendung des \texttt{tiny}-Modells von YOLO zur Bilderkennung, da dieses wesentlich weniger Rechenleistung benötigt.
    \item Kompilation der Bibliotheken mit CPU-Optimierung für den Raspberry Pi und mit GPU-Optimierung für den Jetson Nano
\end{itemize}

Je nach Anwendungsfall kann die Performance noch anhand der folgenden Parameter gesteuert werden:
\begin{itemize}
    \item Eine niedrigere Videoauflösung reduziert die Videoverzögerung und erhöht die Bildrate des Videostreams.
    \item Über den Konfigurationsparameter \texttt{DETECTION\_BLOB\_SIZE} kann die Qualität der Objektdetektionen gesteuert werden.
    Erhöht man diesen Parameter, so verbessert sich die Qualität der Bilderkennung, jedoch steigt auch die benötigte Rechenleistung.
    \item Der Parameter \texttt{TARGET\_FRAMERATE} steuert die maximale Bildrate des Videostreams.
    Mit diesem Wert kann die Latenz des Videos etwas reduziert werden.
    Wird dieser Wert zu hoch gewählt so wird zu viel Rechenleistung aufgebraucht um den Videostream zu verarbeiten.
    Um Rundungsfehler bei der Berechnung der Zeit pro Bild zu vermeiden sollte dieser Wert ein ganzzahliger Teiler von $1000$ sein.
\end{itemize}

Die Standardwerte dieser Parameter in den Setup-Skripten wurden durch Überlegungen und Tests auf Grundlage der obigen Anmerkungen festgelegt.
Sie bieten eine Balance zwischen Bildrate, Verzögerung und Qualität der Bilddetektion, abhängig von der jeweiligen Hardware.
