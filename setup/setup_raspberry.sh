#!/bin/bash
#
###################################################################################
# Dieses Script wurde im Rahmen einer Studienarbeit an der DHBW Mosbach erstellt. #
# Autor: Kilian Krampf                                                            #
# Jahr: 2021/2022                                                                 #
#                                                                                 #
# Dieses Script wurde Raspberry Pi OS Bullseye geschrieben und getestet.          #
###################################################################################

# wrap script in function in case it is executed over the network
function run_script() {
	cat <<-EOF
		Setup für einen Raspberry Pi zur Verwendung in einer Laborumgebung für MINT.
		Diese Einstellungen werden mit ROOT-RECHTEN (!) ausgeführt.

		Dieses Script führt die folgenden Konfigurationen auf einem Raspberry Pi durch:
		  * Einstellung eines deutschen Tastaturlayouts
		  * Aktivierung von SSH
		  * Aktivierung der Kameraschnittstelle
		  * Einrichten einer WLAN-Verbindung
		  * Erhöhung des GPU-Speichers
		  * Aktualisierung des Systems
		  * Installation von docker
		  * Installation und Setup eines DHCP-Servers (ermöglicht eine direkte LAN-Verbindung)
		  * Download/Build der nötigen Docker-Images
		  * Start der nötigen Docker-Container

		Der Ablauf dieses Scripts kann einige Zeit benötigen.

	EOF

	if [ $# -ne 2 ]; then
		cat <<-EOF
			Nutzung des Scripts:
			  ./setup.sh <Wlan-SSID> <Wlan-Passwort>

			Ohne Angabe der Wlan-Daten wird die Netzwerkverbindung nicht automatisch hergestellt.
			Eine Aktualisierung ist gegebenenfalls nicht möglich.

			Das Skript kann dennoch ausgeführt werden, wenn bereits eine Netzwerkverbindung besteht.
			Ansonsten kann die Ausführung innerhalb der nächsten 10 Sekunden abgebrochen werden.

		EOF

		WLAN=0
		sleep 10
	else
		WLAN=1
		SSID=$1
		PASS=$2
	fi

	function do_keyboard() {
		echo "> Konfiguriere deutsches Tastaturlayout... Die Änderungen werden erst nach einem Neustart aktiv."
		cat <<-EOF | sudo tee /etc/default/keyboard >/dev/null 2>/dev/null
			# KEYBOARD CONFIGURATION FILE

			# Consult the keyboard(5) manual page.

			XKBMODEL="pc105"
			XKBLAYOUT="de"
			XKBVARIANT=""
			XKBOPTIONS="lv3:ralt_switch"

			BACKSPACE="guess"
		EOF
	}

	function do_ssh() {
		echo "> Aktiviere SSH-Zugang..."
		sudo raspi-config nonint do_ssh 0 >/dev/null 2>/dev/null
	}

	function do_camera() {
		echo "> Aktiviere Kameraschnittstelle..."
		sudo raspi-config nonint do_legacy 0 >/dev/null 2>/dev/null
	}

	function do_gpu_memory() {
		echo "> Konfiguriere GPU-Speicher..."
		sudo raspi-config nonint do_memory_split 256 >/dev/null 2>/dev/null
	}

	function do_wlan() {
		echo "> Konfiguriere Wlan-Zugang..."
		sudo raspi-config nonint do_wifi_country de >/dev/null 2>/dev/null
		sudo raspi-config nonint do_wifi_ssid_passphrase $1 $2 >/dev/null 2>/dev/null

		# Wait for connection to be established
		COUNTER=0
		while [ $COUNTER -lt 12 ]; do
			ping -c 4 8.8.8.8 >/dev/null 2>/dev/null
			RESULT=$?

			if [ $RESULT -eq 0 ]; then
				COUNTER=12
			else
				echo ". WARN: Internetverbindung besteht noch nicht. Erneuter Test in 10 Sekunden."
				let COUNTER=COUNTER+1
				sleep 10
			fi
		done

		if [ $RESULT -eq 0 ]; then
			echo ". Internetverbindung hergestellt."
			COUNTER=12
		else
			echo ". ERROR: Internetverbindung konnte nicht automatisch hergestellt werden."
			echo ". Falls keine automatische Verbindung hergestellt werden kann, ist es möglich den Internetzugang mithilfe von raspi-config zu konfigurieren und anschließend dieses Script auszuführen."
			echo "! Das Script kann innerhalb der nächsten 30 Sekunden abgebrochen werden !"
			sleep 30
		fi
	}

	function do_update() {
		echo "> Aktualisiere installierte Software..."
		export DEBIAN_FRONTEND=noninteractive
		sudo apt-get update >/dev/null 2>/dev/null
		sudo apt-get full-upgrade -y -qq >/dev/null 2>/dev/null
	}

	function do_install_docker() {
		echo "> Installiere und konfiguriere Docker..."
		curl -fsSL https://get.docker.com | /bin/sh >/dev/null 2>/dev/null
		sudo groupadd docker >/dev/null 2>/dev/null
		sudo usermod -aG docker pi >/dev/null 2>/dev/null
	}

	function do_install_dhcp() {
		echo "> Installiere und konfiguriere DHCP-Server..."
		echo ".   Setze statische IP 192.168.217.1 für eth0"
		cat <<-EOF | sudo tee -a /etc/dhcpcd.conf >/dev/null 2>/dev/null
			interface eth0
			static ip_address=192.168.217.1
		EOF

		echo ".   Installiere DHCP-Server"
		sudo apt-get update >/dev/null 2>/dev/null
		sudo apt-get install isc-dhcp-server -y >/dev/null 2>/dev/null

		echo ".    Konfiguriere DHCP-Server"
		mkdir /tmp >/dev/null 2>/dev/null
		sed {'s/INTERFACESv4=""/INTERFACESv4="eth0"/g'} /etc/default/isc-dhcp-server >/tmp/dhcp_default.tmp
		sudo cp /tmp/dhcp_default.tmp /etc/default/isc-dhcp-server

		cat <<-EOF | sudo tee -a /etc/dhcp/dhcpd.conf >/dev/null 2>/dev/null
			subnet 192.168.217.0 netmask 255.255.255.0 {
			  range 192.168.217.5 192.168.217.50;
			  option subnet-mask 255.255.255.0;
			  option broadcast-address 192.168.217.255;
			}
		EOF

		echo ".    Erstelle Netzwerk-Scripte"
		mkdir ~/network-scripts
		cat <<-EOF >~/network-scripts/dhcp_ethernet.sh
			#!/bin/bash
			#
			###################################################################################
			# Dieses Script wurde im Rahmen einer Studienarbeit an der DHBW Mosbach erstellt. #
			# Autor: Kilian Krampf                                                            #
			# Jahr: 2021/2022                                                                 #
			#                                                                                 #
			# Dieses Script wurde automatisch vom Setup generiert.                            #
			# Es wird verwendet um die Netzwerkeinstellung für Ethernet zurückzusetzen.       #
			###################################################################################

			if [ "\$EUID" -ne 0 ]; then
			   echo "Dieses Script muss als root ausgeführt werden"
			   exit 1
			fi

			function replace_in_file() {
		  	sed {"s/\$2/\$3/g"} \$1 > \$1.tmp
		  	mv \$1.tmp  \$1
			}

			echo "> Deaktiviere DHCP-Server für eth0"
			replace_in_file /etc/default/isc-dhcp-server "INTERFACESv4=\"eth0\"" "INTERFACESv4=\"\""

			echo "> Deaktiviere statische IP für eth0"
			replace_in_file /etc/dhcpcd.conf "interface eth0"                  "#interface eth0"
			replace_in_file /etc/dhcpcd.conf "static ip_address=192.168.217.1" "#static ip_address=192.168.217.1"

			echo "> Konfiguration vollständig. Das System wird in 5 Sekunden neugestartet"
			sleep 5
			sudo reboot
		EOF
		sudo chmod +x ~/network-scripts/dhcp_ethernet.sh

		cat <<-EOF >~/network-scripts/direct_access_ethernet.sh
			#!/bin/bash
			#
			###################################################################################
			# Dieses Script wurde im Rahmen einer Studienarbeit an der DHBW Mosbach erstellt. #
			# Autor: Kilian Krampf                                                            #
			# Jahr: 2021/2022                                                                 #
			#                                                                                 #
			# Dieses Script wurde automatisch vom Setup generiert.                            #
			# Es wird verwendet um die Netzwerkeinstellung für Ethernet für direkten Zugriff  #
			# zu konfigurieren.                                                               #
			###################################################################################

			if [ "\$EUID" -ne 0 ]; then
			   echo "Dieses Script muss als root ausgeführt werden"
			   exit 1
			fi

			function replace_in_file() {
		  	sed {"s/\$2/\$3/g"} \$1 > \$1.tmp
		  	mv \$1.tmp  \$1
			}

			echo "> Deaktiviere DHCP-Server für eth0"
			replace_in_file /etc/default/isc-dhcp-server "INTERFACESv4=\"\"" "INTERFACESv4=\"eth0\""

			echo "> Deaktiviere statische IP für eth0"
			replace_in_file /etc/dhcpcd.conf "#interface eth0"                  "interface eth0"
			replace_in_file /etc/dhcpcd.conf "#static ip_address=192.168.217.1" "static ip_address=192.168.217.1"

			echo "> Konfiguration vollständig. Das System wird in 5 Sekunden neugestartet"
			sleep 5
			sudo reboot
		EOF
		sudo chmod +x ~/network-scripts/direct_access_ethernet.sh
	}

	function do_start_containers() {
		echo "> Erstelle Containerscript mit Standardkonfiguration (Raspberry Camera V2.1)..."
		echo ".   Um die Konfiguration zu ändern kann das Script 'start_containers.sh' angepasst und gestartet werden."
		echo ".   Dieses Script wird im aktuellen Homeverzeichnis generiert..."

		cat <<-EOF >~/start_containers.sh
			#!/bin/bash
			#
			###################################################################################
			# Dieses Script wurde im Rahmen einer Studienarbeit an der DHBW Mosbach erstellt. #
			# Autor: Kilian Krampf                                                            #
			# Jahr: 2021/2022                                                                 #
			#                                                                                 #
			# Dieses Script wurde automatisch vom Setup generiert. Die Einstellungen können   #
			# hier bearbeitet werden.                                                         #
			###################################################################################

			# Set greater than 0 to disable video recording
			DISABLE_RECORDING=0

			# Settings. For Explanations lookup the documentation
			LOG_MAX_LEVEL=DEBUG

			SOURCE_TYPE=WEBCAM
			SOURCE_VIDEO_DEVICE_NO=0
			SOURCE_RES_WIDTH=640
			SOURCE_RES_HEIGHT=480

			YOLO_CONFIG_FILE=/etc/mintenv/yolo/yolo.cfg
			YOLO_WEIGHTS_FILE=/etc/mintenv/yolo/yolo.weights
			YOLO_NAMES_FILE=/etc/mintenv/yolo/yolo.names

			DETECTION_THRESHOLD=0.2
			DETECTION_MAX_OVERLAP=0.4
			DETECTION_BLOB_SIZE=416

			TARGET_RES_WIDTH=\$SOURCE_RES_WIDTH
			TARGET_RES_HEIGHT=\$SOURCE_RES_HEIGHT
			TARGET_FRAMERATE=20

			STREAM_HOST=127.0.0.1
			STREAM_PORT=8554
			STREAM_PATH=mintenv
			STREAM_USER=
			STREAM_PASS=

			function run_script() {
				echo "> Stoppe laufende Container"
				docker stop mintenv_streamserver
				docker stop mintenv_pipeline

				docker container rm mintenv_pipeline
				docker container rm mintenv_streamserver

				echo "> Starte Streamserver"
				docker run -d --net=host --restart=always \\
				           --name=mintenv_streamserver \\
				           -e DISABLE_RECORDING=\$DISABLE_RECORDING \\
				           -v /home/pi/mintenv/recordings:/home/rtsp/video \\
				           menkalian/dhbw_studienarbeit_streamserver:latest

				echo "> Starte Pipeline"

				docker run -d --net=host --restart=always \\
				           --name=mintenv_pipeline \\
				           --device=/dev/video0 \\
				           -e LOG_MAX_LEVEL=\$LOG_MAX_LEVEL \\
				           -e SOURCE_TYPE=\$SOURCE_TYPE \\
				           -e SOURCE_VIDEO_DEVICE_NO=\$SOURCE_VIDEO_DEVICE_NO \\
				           -e SOURCE_RES_WIDTH=\$SOURCE_RES_WIDTH \\
				           -e SOURCE_RES_HEIGHT=\$SOURCE_RES_HEIGHT \\
				           -e YOLO_CONFIG_FILE=\$YOLO_CONFIG_FILE \\
				           -e YOLO_WEIGHTS_FILE=\$YOLO_WEIGHTS_FILE \\
				           -e YOLO_NAMES_FILE=\$YOLO_NAMES_FILE \\
				           -e DETECTION_THRESHOLD=\$DETECTION_THRESHOLD \\
				           -e DETECTION_MAX_OVERLAP=\$DETECTION_MAX_OVERLAP \\
				           -e DETECTION_BLOB_SIZE=\$DETECTION_BLOB_SIZE \\
				           -e TARGET_RES_WIDTH=\$TARGET_RES_WIDTH \\
				           -e TARGET_RES_HEIGHT=\$TARGET_RES_HEIGHT \\
				           -e TARGET_FRAMERATE=\$TARGET_FRAMERATE \\
				           -e STREAM_HOST=\$STREAM_HOST \\
				           -e STREAM_PORT=\$STREAM_PORT \\
				           -e STREAM_PATH=\$STREAM_PATH \\
				           -e STREAM_USER=\$STREAM_USER \\
				           -e STREAM_PASS=\$STREAM_PASS \\
				           menkalian/dhbw_studienarbeit_pipeline:raspi
			}

			run_script
		EOF
		sudo chmod +x ~/start_containers.sh

		echo "> Lade Docker-Images herunter..."
		sudo docker pull menkalian/dhbw_studienarbeit_streamserver:latest
		sudo docker pull menkalian/dhbw_studienarbeit_pipeline:raspi

		echo "> Starte Container..."
		sudo ~/start_containers.sh
	}

	function do_motd() {
		if [ -f "banner_dhbw_colored.txt" ]; then
			sudo mkdir /etc/mintenv
			sudo chmod 777 /etc/mintenv
			cat <<-EOF | sudo tee >/etc/mintenv/show_banner.sh
				printf "$(cat banner_dhbw_colored.txt)"
			EOF
			sudo chmod 755 /etc/mintenv/show_banner.sh
			cat <<-EOF | sudo tee -a /etc/profile >/dev/null 2>/dev/null
			  /etc/mintenv/show_banner.sh
			EOF
		fi
	}

	do_keyboard
	do_ssh
	do_camera
	do_gpu_memory

	if [ $WLAN -eq 1 ]; then
		do_wlan $SSID $PASS
	fi

	do_update
	do_install_docker
	do_install_dhcp

	do_motd
	do_start_containers

	echo "> Setup vollständig. Das System wird in 5 Sekunden neugestartet"
	sleep 5
	sudo reboot
}

run_script "$@"

