#!/bin/bash
#
###################################################################################
# Dieses Script wurde im Rahmen einer Studienarbeit an der DHBW Mosbach erstellt. #
# Autor: Kilian Krampf                                                            #
# Jahr: 2021/2022                                                                 #
#                                                                                 #
# Dieses Script wurde für Jetpack 4.6 erstellt und getestet.                      #
###################################################################################

# wrap script in function in case it is executed over the network
function run_script() {
	cat <<-EOF
		Setup für einen Jetson Nano zur Verwendung in einer Laborumgebung für MINT.
		Diese Einstellungen werden teilweise mit ROOT-RECHTEN (!) ausgeführt.

		Dieses Script führt die folgenden Konfigurationen auf einem Jetson Nano durch:
		  * Aktualisierung des Systems
		  * Setup des DHCP-Servers (ermöglicht eine direkte LAN-Verbindung)
		  * Download der nötigen Docker-Images
		  * Start der nötigen Docker-Container
		  * Erstellung diverser Hilfsscripte

		Der Ablauf dieses Scripts kann einige Zeit benötigen.

	EOF

	function do_network_check() {
		echo "> Teste Internetverbindung"
		# Wait for connection to be established
		COUNTER=0
		while [ $COUNTER -lt 12 ]; do
			ping -c 4 8.8.8.8 >/dev/null 2>/dev/null
			RESULT=$?

			if [ $RESULT -eq 0 ]; then
				COUNTER=12
			else
				echo ". WARN: Internetverbindung besteht noch nicht. Erneuter Test in 10 Sekunden."
				let COUNTER=COUNTER+1
				sleep 10
			fi
		done

		if [ $RESULT -eq 0 ]; then
			echo ". Internetverbindung hergestellt."
		else
			echo ". ERROR: Internetverbindung konnte nicht hergestellt werden."
			echo ".        Eine Ausführung dieses Scripts ohne Netzwerkzugriff wird NICHT empfohlen."
			echo "! Das Script kann innerhalb der nächsten 30 Sekunden abgebrochen werden !"
			sleep 30
		fi
	}

	function do_update() {
		echo "> Aktualisiere installierte Software..."
		export DEBIAN_FRONTEND=noninteractive
		sudo apt-get update
		sudo apt-get full-upgrade -y -qq
		sudo apt-get install -y -qq nano git
	}

	function do_install_docker() {
		echo "> Konfiguriere Docker..."
		sudo groupadd docker               >/dev/null 2>/dev/null
		sudo usermod -aG docker $(whoami)  >/dev/null 2>/dev/null
	}

	function do_install_dhcp() {
		echo "> Konfiguriere DHCP-Server..."

		# Set DHCP configuration before enabling the dhcp-server
		cat <<-EOF | sudo tee -a /etc/dhcp/dhcpd.conf >/dev/null 2>/dev/null
			subnet 192.168.217.0 netmask 255.255.255.0 {
			  range 192.168.217.5 192.168.217.50;
			  option subnet-mask 255.255.255.0;
			  option broadcast-address 192.168.217.255;
			}
		EOF

		echo ". Bereite statische IP zur späteren Verwendung vor"
		cat <<-EOF | sudo tee -a /etc/network/interfaces >/dev/null 2>/dev/null
			#auto eth0
			#iface eth0 inet static
			  #address 192.168.217.1
			  #netmask 255.255.255.0
		EOF

		echo ".    Erstelle Netzwerk-Scripte"
		mkdir ~/network-scripts
		cat <<-EOF >~/network-scripts/dhcp_ethernet.sh
			#!/bin/bash
			#
			###################################################################################
			# Dieses Script wurde im Rahmen einer Studienarbeit an der DHBW Mosbach erstellt. #
			# Autor: Kilian Krampf                                                            #
			# Jahr: 2021/2022                                                                 #
			#                                                                                 #
			# Dieses Script wurde automatisch vom Setup generiert.                            #
			# Es wird verwendet um die Netzwerkeinstellung für Ethernet zurückzusetzen.       #
			###################################################################################

			if [ "\$EUID" -ne 0 ]; then
			   echo "Dieses Script muss als root ausgeführt werden"
			   exit 1
			fi

			function replace_in_file() {
		  	sed {"s/\$2/\$3/g"} \$1 > \$1.tmp
		  	mv \$1.tmp  \$1
			}

			echo "> Deaktiviere DHCP-Server für eth0"
			replace_in_file /etc/default/isc-dhcp-server "INTERFACESv4=\"eth0\"" "INTERFACESv4=\"\""

			echo "> Deaktiviere statische IP für eth0"
			replace_in_file /etc/network/interfaces "auto eth0"              "#auto eth0"
			replace_in_file /etc/network/interfaces "iface eth0 inet static" "#iface eth0 inet static"
			replace_in_file /etc/network/interfaces "address 192.168.217.1"  "#address 192.168.217.1"
			replace_in_file /etc/network/interfaces "netmask 255.255.255.0"  "#netmask 255.255.255.0"

			systemctl disable isc-dhcp-server

			echo "> Konfiguration vollständig. Das System wird in 5 Sekunden neugestartet"
			sleep 5
			reboot
		EOF
		sudo chmod +x ~/network-scripts/dhcp_ethernet.sh

		cat <<-EOF >~/network-scripts/direct_access_ethernet.sh
			#!/bin/bash
			#
			###################################################################################
			# Dieses Script wurde im Rahmen einer Studienarbeit an der DHBW Mosbach erstellt. #
			# Autor: Kilian Krampf                                                            #
			# Jahr: 2021/2022                                                                 #
			#                                                                                 #
			# Dieses Script wurde automatisch vom Setup generiert.                            #
			# Es wird verwendet um die Netzwerkeinstellung für Ethernet zurückzusetzen.       #
			###################################################################################

			if [ "\$EUID" -ne 0 ]; then
			   echo "Dieses Script muss als root ausgeführt werden"
			   exit 1
			fi

			function replace_in_file() {
		  	sed {"s/\$2/\$3/g"} \$1 > \$1.tmp
		  	mv \$1.tmp  \$1
			}

			echo "> Aktiviere DHCP-Server für eth0"
			replace_in_file /etc/default/isc-dhcp-server "INTERFACESv4=\"eth0\"" "INTERFACESv4=\"\""

			echo "> Aktiviere statische IP für eth0"
			replace_in_file /etc/network/interfaces "#auto eth0"              "auto eth0"
			replace_in_file /etc/network/interfaces "#iface eth0 inet static" "iface eth0 inet static"
			replace_in_file /etc/network/interfaces "#address 192.168.217.1"  "address 192.168.217.1"
			replace_in_file /etc/network/interfaces "#netmask 255.255.255.0"  "netmask 255.255.255.0"

			systemctl enable isc-dhcp-server

			echo "> Konfiguration vollständig. Das System wird in 5 Sekunden neugestartet"
			sleep 5
			reboot
		EOF
		sudo chmod +x ~/network-scripts/direct_access_ethernet.sh
	}

	function do_start_containers() {
		echo "> Erstelle Containerscript mit Standardkonfiguration (ZED2)..."
		echo ".   Um die Konfiguration zu ändern kann das Script 'start_containers.sh' angepasst und gestartet werden."
		echo ".   Dieses Script wird im aktuellen Homeverzeichnis generiert..."

		cat <<-EOF >~/start_containers.sh
			#!/bin/bash
			#
			###################################################################################
			# Dieses Script wurde im Rahmen einer Studienarbeit an der DHBW Mosbach erstellt. #
			# Autor: Kilian Krampf                                                            #
			# Jahr: 2021/2022                                                                 #
			#                                                                                 #
			# Dieses Script wurde automatisch vom Setup generiert. Die Einstellungen können   #
			# hier bearbeitet werden.                                                         #
			###################################################################################

			# Set greater than 0 to disable video recording
			DISABLE_RECORDING=0

			# Settings. For Explanations lookup the documentation
			LOG_MAX_LEVEL=DEBUG

			SOURCE_TYPE=ZED2
			SOURCE_VIDEO_DEVICE_NO=0
			SOURCE_RES_WIDTH=1280
			SOURCE_RES_HEIGHT=720

			YOLO_CONFIG_FILE=/etc/mintenv/yolo/yolo.cfg
			YOLO_WEIGHTS_FILE=/etc/mintenv/yolo/yolo.weights
			YOLO_NAMES_FILE=/etc/mintenv/yolo/yolo.names

			DETECTION_THRESHOLD=0.2
			DETECTION_MAX_OVERLAP=0.4
			DETECTION_BLOB_SIZE=832

			TARGET_RES_WIDTH=\$SOURCE_RES_WIDTH
			TARGET_RES_HEIGHT=\$SOURCE_RES_HEIGHT
			TARGET_FRAMERATE=40

			STREAM_HOST=127.0.0.1
			STREAM_PORT=8554
			STREAM_PATH=mintenv
			STREAM_USER=
			STREAM_PASS=

			function run_script() {
				echo "> Stoppe laufende Container"
				docker stop mintenv_streamserver                                                               > /dev/null 2>/dev/null
				docker stop mintenv_pipeline                                                                   > /dev/null 2>/dev/null

				docker container rm mintenv_pipeline                                                           > /dev/null 2>/dev/null
				docker container rm mintenv_streamserver                                                       > /dev/null 2>/dev/null

				echo "> Starte Streamserver"
				docker run -d --net=host --restart=always \\
				           --name=mintenv_streamserver \\
				           -e DISABLE_RECORDING=\$DISABLE_RECORDING \\
				           -v ~/mintenv/recordings:/home/rtsp/video \\
				           menkalian/dhbw_studienarbeit_streamserver:latest

				echo "> Starte Pipeline"
				docker run -d --net=host --restart=always \\
				           --name=mintenv_pipeline \\
				           --privileged \\
				           -v ~/mintenv/zed2_calibrations:/usr/local/zed/settings \\
				           -e LOG_MAX_LEVEL=\$LOG_MAX_LEVEL \\
				           -e SOURCE_TYPE=\$SOURCE_TYPE \\
				           -e SOURCE_VIDEO_DEVICE_NO=\$SOURCE_VIDEO_DEVICE_NO \\
				           -e SOURCE_RES_WIDTH=\$SOURCE_RES_WIDTH \\
				           -e SOURCE_RES_HEIGHT=\$SOURCE_RES_HEIGHT \\
				           -e YOLO_CONFIG_FILE=\$YOLO_CONFIG_FILE \\
				           -e YOLO_WEIGHTS_FILE=\$YOLO_WEIGHTS_FILE \\
				           -e YOLO_NAMES_FILE=\$YOLO_NAMES_FILE \\
				           -e DETECTION_THRESHOLD=\$DETECTION_THRESHOLD \\
				           -e DETECTION_MAX_OVERLAP=\$DETECTION_MAX_OVERLAP \\
				           -e DETECTION_BLOB_SIZE=\$DETECTION_BLOB_SIZE \\
				           -e TARGET_RES_WIDTH=\$TARGET_RES_WIDTH \\
				           -e TARGET_RES_HEIGHT=\$TARGET_RES_HEIGHT \\
				           -e TARGET_FRAMERATE=\$TARGET_FRAMERATE \\
				           -e STREAM_HOST=\$STREAM_HOST \\
				           -e STREAM_PORT=\$STREAM_PORT \\
				           -e STREAM_PATH=\$STREAM_PATH \\
				           -e STREAM_USER=\$STREAM_USER \\
				           -e STREAM_PASS=\$STREAM_PASS \\
				           menkalian/dhbw_studienarbeit_pipeline:jetson
			}

			run_script
		EOF
		sudo chmod +x ~/start_containers.sh

		echo "> Lade Docker-Images herunter"
		sudo docker pull menkalian/dhbw_studienarbeit_streamserver:latest
		sudo docker pull menkalian/dhbw_studienarbeit_pipeline_env:jetson
		sudo docker pull menkalian/dhbw_studienarbeit_pipeline:jetson

		echo "> Starte Container..."
		sudo ~/start_containers.sh
	}

	function do_motd() {
		if [ -f "banner_dhbw_colored.txt" ]; then
			sudo mkdir /etc/mintenv
			sudo chmod 777 /etc/mintenv
			cat <<-EOF | sudo tee >/etc/mintenv/show_banner.sh
				printf "$(cat banner_dhbw_colored.txt)"
			EOF
			sudo chmod 755 /etc/mintenv/show_banner.sh
			cat <<-EOF | sudo tee -a /etc/profile >/dev/null 2>/dev/null
			  /etc/mintenv/show_banner.sh
			EOF
		fi
	}

	do_network_check
	do_update
	do_install_docker
	do_install_dhcp

	do_motd
	do_start_containers

	echo "> Lade Projektdateien von Gitlab"
	git clone https://gitlab.com/kiliankra/dhbw_studienarbeit_sources.git ~/dhbw_studienarbeit_sources

	echo "> Setup vollständig. Das System wird in 5 Sekunden neugestartet"
	sleep 5
	sudo reboot
}

run_script "$@"

