# Trainingsumgebung MINT und AI (Artificial Intelligence)

## Abstract
Diese Studienarbeit befasst sich mit dem Setup einer Trainingsumgebung. Diese kann genutzt werden, um Schüler*innen der
Oberstufe mit den Themen künstliche Intelligenz und Bilderkennung vertraut zu machen.Das Ziel dieser Trainingsumgebung
ist es, eine Objekterkennung auf das Bild einer Kamera anzuwenden, die erkannten Objekte zu markieren und das
resultierende Video als Stream zur Verfügung zu stellen. Zur Bilddetektion wird das YOLO Modell verwendet, das mit dem
COCO-Dataset und dem OpenImages-Dataset trainiert wurde. Zur Bereitstellung des Videostreams wird GStreamer mit dem
RTSP-Protokoll verwendet. Diese Trainingsumgebung wird für zwei Hardwarekonfigurationen bereitgestellt: Entweder ein
Raspberry Pi mit einem passenden Kameramodul oder ein NVIDIA Jetson Nano mit einer ZED2 3D-Kamera. Die finale
Implementierung wird in Docker-Images mit Hardware-Integration eingesetzt. Das Setup wurde zudem auf die Verwendung in
einem Unterrichtsumfeld optimiert, indem einige Möglichkeiten zum Experimentieren und Erkunden des technischen
Setups gegeben werden. Ein Beispiel dafür sind die Optimierung der Docker-Images für eine kurze Buildzeit oder der
einfache Austausch des trainierten Modells für die Bildererkennung.

## Technische Dokumentation
Die vollständige Dokumentation befindet sich [hier](docs/technische_dokumentation.pdf)

## Kontakt

Für Fragen (insbesondere falls du ein/e Student*in bist, der/die eine Studienarbeit auf Basis meiner Ergebnisse entwickelst 😉) stehe ich unter der E-Mail kiliankra@gmail.com zur Verfügung.
