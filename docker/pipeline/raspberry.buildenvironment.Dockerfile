#################################################################################
# Diese Datei wurde im Rahmen einer Studienarbeit an der DHBW Mosbach erstellt. #
# Autor: Kilian Krampf                                                          #
# Jahr: 2021/2022                                                               #
#                                                                               #
# Diese Datei erwartet das Wurzelverzeichnis des Projekts als Build-Kontext.    #
# Dies kann beispielsweise erreicht werden indem folgender Befehl (im Wurzel-   #
# verzeichnis) verwendet wird:                                                  #
# $ docker build -t menkalian/dhbw_studienarbeit_pipeline_env:raspi \           #
#                -f docker/pipeline/raspberry.buildenvironment.Dockerfile .     #
#                                                                               #
# Ohne Caching (bzw. bei der ersten Ausführung) dauert das Bauen des Images     #
# recht lange (2h+).                                                            #
#################################################################################

FROM debian:bullseye-slim

WORKDIR /home/root

# Install packages in container and clear APT-Cache afterwards
RUN apt-get update && apt-get install -y \
    git make cmake gcc g++ pkg-config \
    libgstreamer1.0-dev  libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev  \
    gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly \
    gstreamer1.0-rtsp gstreamer1.0-tools gstreamer1.0-gl gstreamer1.0-gtk3 ; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Installation von OpenCV mit CPU-Optimierung
RUN git clone https://github.com/opencv/opencv.git ; \
    mkdir opencv/cmake-build-dir ; cd opencv/cmake-build-dir ; \
    cmake -D With_GStreamer=ON -D ENABLE_NEON=ON -D ENABLE_VFPV3=ON -D OPENCV_ENABLE_NONFREE=ON -D CMAKE_SHARED_LINKER_FLAGS='-latomic' .. ; \
    make -j$(nproc) ; make install ; \
    cd /home/root ; rm -rf opencv
