#################################################################################
# Diese Datei wurde im Rahmen einer Studienarbeit an der DHBW Mosbach erstellt. #
# Autor: Kilian Krampf                                                          #
# Jahr: 2021/2022                                                               #
#                                                                               #
# Diese Datei erwartet das Wurzelverzeichnis des Projekts als Build-Kontext.    #
#                                                                               #
# Um dieses Image bauen zu können muss das Git-Repository                       #
# gitlab.com/kiliankra/dhbw_studienarbeit_cuda_bin im Verzeichnis cuda          #
# ausgeckeckt werden.                                                           #
# Am einfachsten ist dies zu erreichen indem das Haupt-Repository mit           #
# `--recurse-submodules` ausgecheckt wird.                                      #
#                                                                               #
# Dies kann beispielsweise erreicht werden indem folgender Befehl (im Wurzel-   #
# verzeichnis) verwendet wird:                                                  #
# $ docker build -t menkalian/dhbw_studienarbeit_pipeline_env:jetson \          #
#                -f docker/pipeline/jetson.buildenvironment.Dockerfile .        #
#                                                                               #
# Ohne Caching (bzw. bei der ersten Ausführung) dauert das Bauen des Images     #
# recht lange (4h+).                                                            #
#################################################################################

FROM stereolabs/zed:3.6-devel-jetson-jp4.6

WORKDIR /home/root

# Install packages in container and clear APT-Cache afterwards
RUN apt-get update && apt-get install -y \
    git make cmake gcc g++ pkg-config libeigen3-dev libv4l-dev libusb-1.0-0-dev \
    libgstreamer1.0-dev  libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev  \
    gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly \
    gstreamer1.0-rtsp gstreamer1.0-tools gstreamer1.0-gl gstreamer1.0-gtk3 ; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Installation von CMake
RUN wget https://github.com/Kitware/CMake/releases/download/v3.22.2/cmake-3.22.2-linux-aarch64.sh ; \
    chmod +x cmake-3.22.2-linux-aarch64.sh ; \
    ./cmake-3.22.2-linux-aarch64.sh --skip-license --prefix=/usr

# Copy CUDA-Binaries into the image
ADD docker/pipeline/cuda/cuda_bin.tar.gz /

# Installation von OpenCV mit GPU-Optimierung
RUN git clone https://github.com/opencv/opencv.git ; \
    git clone https://github.com/opencv/opencv_contrib.git ; \
    cd opencv ; git checkout 4.5.5 ; cd .. ; \
    cd opencv_contrib ; git checkout 4.5.5 ; cd .. ; \
    mkdir opencv/cmake-build-dir ; cd opencv/cmake-build-dir ; \
     cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr -D OPENCV_EXTRA_MODULES_PATH=/home/root/opencv_contrib/modules -D EIGEN_INCLUDE_PATH=/usr/include/eigen3 -D WITH_OPENCL=OFF -D WITH_CUDA=ON -D WITH_CUDNN=ON -D WITH_CUBLAS=ON -D ENABLE_FAST_MATH=ON -D CUDA_FAST_MATH=ON -D OPENCV_DNN_CUDA=ON -D ENABLE_NEON=ON -D WITH_OPENMP=ON -D WITH_GSTREAMER=ON -D WITH_EIGEN=ON -D OPENCV_ENABLE_NONFREE=ON -D OPENCV_GENERATE_PKGCONFIG=ON .. ; \
    make -j$(nproc) ; make install
