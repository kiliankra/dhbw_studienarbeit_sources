#################################################################################
# Diese Datei wurde im Rahmen einer Studienarbeit an der DHBW Mosbach erstellt. #
# Autor: Kilian Krampf                                                          #
# Jahr: 2021/2022                                                               #
#                                                                               #
# Diese Datei erwartet das Wurzelverzeichnis des Projekts als Build-Kontext.    #
# Dies kann beispielsweise erreicht werden indem folgender Befehl (im Wurzel-   #
# verzeichnis) verwendet wird:                                                  #
# $ docker build -t menkalian/dhbw_studienarbeit_pipeline:raspi \           #
#                -f docker/pipeline/raspberry.Dockerfile .                      #
#                                                                               #
# Zum Bauen wird das "buildenvironment" Image benötigt. Dieses wird auf         #
# docker-hub gesucht und von dort heruntergeladen.                              #
#################################################################################

FROM menkalian/dhbw_studienarbeit_pipeline_env:raspi

# Copy sources for the pipeline and compile it
COPY src /home/root/lab_environment_src
RUN mkdir lab_environment_src/cmake-build-dir ; cd lab_environment_src/cmake-build-dir ; \
    cmake -DCMAKE_BUILD_TYPE=Release .. ; \
    make -j$(nproc) ; cd /home/root ; \
    cp lab_environment_src/cmake-build-dir/lab_environment lab_environment ; \
    rm -rf lab_environment_src

# Add the latest YOLO-Files
COPY training/results/latest /etc/mintenv/yolo

CMD [ "/home/root/lab_environment" ]
