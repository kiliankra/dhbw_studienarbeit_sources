#!/bin/sh
#

# Allow disabling the recording
if [ "${DISABLE_RECORDING:-0}" -gt "0" ]; then
        exit 0
fi

ffmpeg -rtsp_transport tcp -i rtsp://localhost:$1/$2 -c copy -segment_time 00:30:00 -f segment $(date +/home/rtsp/video/saved_%Y-%m-%d_%H-%M-%S_seg%%03d.mp4)
exit $?